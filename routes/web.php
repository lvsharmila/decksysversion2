<?php

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;



use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('main.index');
// });

Route::get('/', function () {
    return view('main.index');
});

Route::get('/test12', function () {
    return view('main.test12');
});


Route::get('/ManagedServices', function () {
    return view('main.ManagedServices');
});

Route::get('/support', function () {
    return view('main.support');
});

Route::get('/locations', function () {
    return view('main.locations');
});

// Route::get('/DedicatedServer', function () {
//     return view('main.DedicatedServer');
// });

Route::get('/Register', function () {
    return view('main.Register');
});

Route::get('/login', function () {
    return view('main.login');
});
// Route::get('/test', function () {
//     return view('main.test');
// });


Route::any('/addtocart', 'CartController@addCart')->name('cart.add');
Route::any('/cart', 'CartController@cart_view')->name('cart');
//  Route::any('/cart', 'CartController@show1');

Route::any('/VPS', 'ProductController@show');
Route::any('/DedicatedServer', 'ProductController@show2');
Route::post('insert','RegisterController@insert')->name('user.insert');

Route::any('login', 'LoginController@signin');
Route::post('signin_action','LoginController@signin_action')->name('login.signin_action');
Route::get('signout', 'LoginController@signout')->name('logout.signout');
Route::get('/reset_password','LoginController@reset_password');
Route::post('reset_password_action','LoginController@reset_password_action')->name('login.reset_password_action');
Route::post('forgotpassword_action','LoginController@forgotpassword_action')->name('login.forgotpassword_action');

Route::get('/clientlayout.main.index','IndexController@get');


Route::get('/Myservices','MyServicesController@show');


Route::get('/Services','ServicesController@show1');

Route::get('/Mydomains','MyDomainsController@getdomain');

Route::get('/Invoices','InvoicesController@payment');

Route::get('/Orders','OrdersController@orders');

Route::any('/Funds', 'FundsController@Funds')->name('Funds.success');

Route::get('/Currency','CurrencyController@rate');

Route::get('Myprofile', function () {
    return view('clientlayout.main.Myprofile');
});
Route::get('Mycontacts', function () {
    return view('clientlayout.main.Mycontacts');
});
Route::get('Password', function () {
    return view('clientlayout.main.Password');
});

Route::get('/forgotpassword', function () {
    return view('main.forgotpassword');
});

// Route::get('Register', function () {
//     return view('clientlayout.main.Register');
// });

 Route::get('/Currency',function(){
 $currency=WHMCS::GetOrders([
 ]);
echo "<pre>"; print_r($currency);
 return view('main.Currency');
 });