<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;


class CartController extends Controller
{
    public function addToCart(){
    return Cart::content();
    }

	
	

	
    public function addCart(Request $request)
    {
         //print_r(Input::get());exit;
        $name = $request->input();
        $os = $request->input();
        $db = $request->input();
        $total = $request->input();
        $gid = $request->input();
        
  
	

// echo "<pre>";print_r($name);exit;

        $response=Cart::add(['id'=>str_random(50) ,'name' => $name, 'qty' => 1, 'price' => $total,  'options' => ['package' => 'Dedicated Server', 'gid' => $gid,'db' => $db, 'os' => $os,]]);
		//echo "<pre>";print_r($response);exit;
		//echo "<pre>";print_r($response);exit;
       
        $data=json_encode($response);
        $cartdata=json_decode($data);
		session()->put('transfer','true');
        return redirect('cart');


    } 
    public function cart_view()
    {
        $data=json_encode(Cart::content());
        $cartdata=json_decode($data);
        //echo "<pre>";print_r($cartdata);exit;
	

        return view('main.cart',compact('cartdata','response'));
    }
	
	

    public function cart_remove(Request $request)
    {

        Cart::remove($request->rowid);
        return redirect('cart');

    }
	
	
}
