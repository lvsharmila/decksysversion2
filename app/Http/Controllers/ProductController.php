<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\View;

use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        $products = Whmcs::GetProducts([


        ]);
        return view('main.VPS', compact('products'));
    }

    public function show2()
    {

        $products = Whmcs::GetProducts([


        ]);
        return view('main.DedicatedServer', compact('products'));
    }

}
