<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="client/lib/animate.css" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="client/lib/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="client/lib/bootstrap.css" type="text/css" />
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <link rel="stylesheet" href="client/css/font_styling.css" type="text/css" />
    <link rel="stylesheet" href="client/css/app.css" type="text/css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
<script type="text/javascript" src="client/js/StatesDropdown1.js"></script>

    <!--<link rel="stylesheet" href="client/css/style1.css">-->


    @yield('styles')
</head>
<body>
@include('partials.header')

@yield('content')



@yield('scripts')

<script src="client/js/jquery.js"></script>
<script src="client/js/bootstrap.min.js"></script>
<script src="client/js/ui-load.js"></script>
<script src="client/js/ui-jp.config.js"></script>
<script src="client/js/ui-jp.js"></script>
<script src="client/js/ui-nav.js"></script>
<script src="client/js/ui-toggle.js"></script>
<script src="client/js/ui-client.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script  src="client/js/index.js"></script>

<script>
    $(document).ready( function () {
        $('table').DataTable();
    } );
    var myObj, x;
    myObj = $response;
    x = myObj.status;
    document.getElementById("demo").innerHTML = x;

</script>

<!--<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>-->

</body>
</html>