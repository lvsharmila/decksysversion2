<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="client/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="client/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="client/css/simple-line-icons.css" type="text/css" />
    <!-- <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />  -->
    <link rel="stylesheet" href="client/css/style.css" type="text/css" />
    <link rel="stylesheet" href="client/css/bootstrap.min.css" type="text/css" />
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" type="text/css" /> -->
	

    <link rel="stylesheet" href="client/css/font.css" type="text/css" />
    <link rel="stylesheet" href="client/css/app.css" type="text/css" />
    
   
    <style>
		#contact label{
			
			width: 100px;
			text-align: right;
		}
		#contact_submit{
			padding-left: 100px;
		}
		#contact div{
			margin-top: 0em;
		}
		textarea{
			vertical-align: top;
			height: 5em;
		}
			
		.error{
			display: none;
			margin-left: 10px;
		}		
		
		.error_show{
			color: red;
			margin-left: 10px;
		}
		
		input.invalid, textarea.invalid{
			border: 2px solid red;
		}
		
		input.valid, textarea.valid{
			border: 2px solid green;
		}
	</style>
    

    <!--<link rel="stylesheet" href="client/css/style1.css">-->


    @yield('styles')
</head>
<body>
@include('clientlayout.partials.header')


@yield('content')



@yield('scripts')


<script src="client/js/jquery.js"></script>
<script src="client/js/bootstrap.js"></script>
<script src="client/js/ui-load.js"></script>
<script src="client/js/ui-jp.config.js"></script>
<script src="client/js/ui-jp.js"></script>
<script src="client/js/ui-nav.js"></script>
<script src="client/js/ui-toggle.js"></script>
<script src="client/js/ui-client.js"></script>
<script src="client/js/StatesDropdown.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
		$(document).ready(function() {
			<!-- Real-time Validation -->
				<!--Name can't be blank-->
				$('#contact_name').on('input', function() {
					var input=$(this);
					var is_name=input.val();
					if(is_name){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
				
				
				<!--Email must be an email -->
				$('#contact_email').on('input', function() {
					var input=$(this);
					var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
					var is_email=re.test(input.val());
					if(is_email){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
				
				<!--Website must be a website -->
				$('#contact_website').on('input', function() {
					var input=$(this);
					if (input.val().substring(0,4)=='www.'){input.val('http://www.'+input.val().substring(4));}
					var re = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;
					var is_url=re.test(input.val());
					if(is_url){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
				
				<!--Message can't be blank -->
				$('#contact_message').keyup(function(event) {
					var input=$(this);
					var message=$(this).val();
					console.log(message);
					if(message){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}	
				});


						
			<!-- After Form Submitted Validation-->
			$("#contact_submit button").click(function(event){
				var form_data=$("#contact").serializeArray();
				var error_free=true;
				for (var input in form_data){
					var element=$("#contact_"+form_data[input]['name']);
					var valid=element.hasClass("valid");
					var error_element=$("span", element.parent());
					if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
					else{error_element.removeClass("error_show").addClass("error");}
				}
				if (!error_free){
					event.preventDefault(); 
				}
				else{
					alert('No errors: Form will be submitted');
				}
			});
			
			
			
		});




	
	</script>




<script>
		$(document).ready(function() {
			<!-- Real-time Validation -->
				<!--Name can't be blank-->
				$('#contact_firstname').on('input', function() {
					var input=$(this);
					var is_firstname=input.val();
					if(is_firstname){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
				
				$('#contact_lastname').on('input', function() {
					var input=$(this);
					var is_lastname=input.val();
					if(is_lastname){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
				
				<!--Email must be an email -->
				$('#contact_email').on('input', function() {
					var input=$(this);
					var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
					var is_email=re.test(input.val());
					if(is_email){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_address1').on('input', function() {
					var input=$(this);
					var is_address1=input.val();
					if(is_address1){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_address2').on('input', function() {
					var input=$(this);
					var is_address2=input.val();
					if(is_address2){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_city').on('input', function() {
					var input=$(this);
					var is_city=input.val();
					if(is_city){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_state').on('input', function() {
					var input=$(this);
					var is_state=input.val();
					if(is_state){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_postcode').on('input', function() {
					var input=$(this);
					var is_postcode=input.val();
					if(is_postcode){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_password').on('input', function() {
					var input=$(this);
					var is_password=input.val();
					if(is_password){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_password1').on('input', function() {
					var input=$(this);
					var is_password1=input.val();
					if(is_password1){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});

				$('#contact_agree').on('input', function() {
					var input=$(this);
					var is_agree=input.val();
					if(is_agree){input.removeClass("invalid").addClass("valid");}
					else{input.removeClass("valid").addClass("invalid");}
				});
						
			<!-- After Form Submitted Validation-->
			$("#contact_submit button").click(function(event){
				var form_data=$("#contact").serializeArray();
				var error_free=true;
				for (var input in form_data){
					var element=$("#contact_"+form_data[input]['name']);
					var valid=element.hasClass("valid");
					var error_element=$("span", element.parent());
					if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
					else{error_element.removeClass("error_show").addClass("error");}
				}
				if (!error_free){
					event.preventDefault(); 
				}
				else{
					alert('No errors: Form will be submitted');
				}
			});
			
			
			
		});




	
	</script>
<script>
    $(document).ready( function () {
        $('table').DataTable();
    } );
    var myObj, x;
    myObj = $response;
    x = myObj.status;
    document.getElementById("demo").innerHTML = x;

</script>




</body>
</html>
