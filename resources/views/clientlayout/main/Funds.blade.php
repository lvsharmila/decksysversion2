@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Funds
@endsection

@section('content')


	<div id="content" class="app-content" role="main">
		<div class="app-content-body ">
			<div class="bg-light lter b-b wrapper-md">
				<h1 class="m-n font-thin h3">Funds</h1>
			</div>
				<div class="wrapper-md">
					<div >
						<div class="text-center">
						<?php if(isset($_GET['error'])){ ?>
                <p style="text-align: center;color: red;font-size: 18px;">Funds Added successfully</p>
                <?php } ?>
							
									  <h2>Credit Balance - @foreach($balance  as $key=> $invoice)
								@if($key=='credit')
								{{$invoice}}
								@endif
								@endforeach </h2>
						</div>
					</div>
				</div>
				<div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center" style="padding-bottom: 20px;">
                            <button class="btn btn-info btn-lg" style="margin-left: 50px" type="submit" data-toggle="modal" data-target="#myModalz" >Add Funds</button>
                        </div>
                    </div>

		</div>
	</div>

	

      
    
<div class="container">
 

<!-- Modal -->
  <div class="modal fade" id="myModalz" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content2">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center">Add Credit Balance</h3>
        </div>
        <div class="modal-body">
          <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
       
        <div class="panel-body">
		
                
          <form role="form" action="{{route('Funds.success')}}" method="POST">
		    <input type = "hidden" name = "_token" value = "{{csrf_token()}}">
            <div class="form-group">
              <label>Date</label>
              <input type="text" name="domainname" class="form-control" value="{{date("d/m/Y")}}" placeholder="Enter Your Domain Name">
            </div>
            <div class="form-group">
              <label>Description</label>
              <input type="text" name="description" class="form-control" placeholder="Description">
            </div>

<div class="form-group">
              <label>Amount</label>
              <input type="text" name="amount" class="form-control" placeholder="Credit Balance">
            </div>
            
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </div>
        </div>
        
      </div>
      
    </div>
  </div>
  


@endsection