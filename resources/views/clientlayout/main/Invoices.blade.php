@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Invoice
@endsection

@section('content')


<!-- {{print_r($response)}} -->
<div id="content" class="app-content" role="main">
  <div class="app-content-body ">
    <div class="bg-light lter b-b wrapper-md">
      <h1 class="m-n font-thin h1 block">My Invoicess</h1>
    </div>
    <div class="wrapper-md">
      <div class="panel panel-default">
        <div class="table-responsive">
          <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
            <div class="row"><div class="col-sm-12">
              <table ui-jq="dataTable" ui-options="{
                    sAjaxSource: 'api/datatable.json',
                    aoColumns: [
                      { mData: 'engine' },
                      { mData: 'browser' },
                      { mData: 'platform' },
                      { mData: 'version' },
                      { mData: 'grade' }
                    ]
                  }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                <thead>
                  <tr role="row">
                    <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">Invoice ID</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Invoice Number</th>
                    <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Invoice Date</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Due Date</th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Total Amount</th>
                    <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Status </th>
                  </tr>
                  </thead>
                  <tbody>
                    @if($response['totalresults'] == '0')
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>
                        <td> - </td>

                    @else
                        @foreach($response['invoices']['invoice'] as $key=>$value)
                          @for($key=0;$key<100;$key++)
                          @endfor
                            <tr role="row">
                              <td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">
                                <a href=""data-toggle="modal" data-target="#myModal" style="color:#23b7e5">{{$value['id']}}</a></td>
                              <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['invoicenum']}}</td>
                              <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['datepaid']}}</td>
                              <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['duedate']}}</td>
                              <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$value['total']}}</td>
                              <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">
                              @if($value['status']=='Paid')
                                <button class="btn btn-success btn-md" style="padding: 5px 30px 5px 30px;">Paid</button>  
                              @elseif($value['status']=='Unpaid')
                                <button class="btn btn-danger btn-md" style="padding: 5px 20px 5px 20px; ">Unpaid</button>
                              @endif 
                              </td>
                            </tr>                  
                      @endforeach
                    @endif
                </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">  
        <!-- Trigger the modal with a button -->
        <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
        <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">    
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title">Invoice</h4>
            </div>
            <div class="modal-body">
              <p>Your Invoice</p>
            </div>       
          </div>      
        </div>
      </div>  
    </div>
    @endsection