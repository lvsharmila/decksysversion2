@extends('clientlayout.layouts.master')

@section('title')
  Decksys | My Services
@endsection

@section('content')

<?php //echo "<pre>";print_r($value);exit;?>

  
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
      <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h1 block">My Services</h1>
      </div>
      <div class="wrapper-md">
        <div class="panel panel-default">
          <div class="table-responsive">
            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
              <div class="row"><div class="col-sm-12">
                <table ui-jq="dataTable" ui-options="{
                  sAjaxSource: 'api/datatable.json',
                  aoColumns: [
                    { mData: 'engine' },
                    { mData: 'browser' },
                    { mData: 'platform' },
                    { mData: 'version' },
                    { mData: 'grade' }
                  ]
                }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
              <thead>
                <tr role="row">
                  <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">Products / Services</th>
                  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">IP Addresss</th>
                  <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Billing Cycle</th>
                  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Next DueDate</th>
                  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Status</th>
                  <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
                </tr>
              </thead>    
              <tbody>
              @if($value['totalresults'] == '0')
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
                  <td> - </td>
              @else
                @foreach($value['products']['product'] as $key)
                  <tr role="row">
                    <!-- <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">{{$key['id']}}</td> -->
                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">          
                    <!--<a href="" style="color:#23b7e5" data-toggle="modal" data-target="#myModal">-->{{$key['groupname']}} - {{$key['name']}}</td>
                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['serverip']}}</td>
                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['billingcycle']}}</td>
                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['nextduedate']}}</td>
                    <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$key['status']}}</td>
                    <td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">
                      <button class="btn btn-primary btn-sm" style="padding: 5px 20px 5px 20px;">Cancel order</button>  
                    </td>
                  </tr>         
                @endforeach
            @endif
            </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>    
  </div>
  @endsection 