@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Orders
@endsection

@section('content')

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h1 block">My Orders</h1>
            </div>
            <div class="wrapper-md">
                <div class="panel panel-default">
                
    <div class="table-responsive">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

<div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
                                        <tr role="row">
                                        
                                            <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">Order Number</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Order Date</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Cost</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Order Status</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action</th>
                                            
                                        </tr>
                                        </thead>  <tbody>
                                        
                                        @if($orders['totalresults']=='0')
			<td> - </td>
			<td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
		@else
			@foreach($orders['orders']['order'] as $k => $data)
		@for($k=0;$k<100;$k++)
		@endfor
		
        <tr role="row">
       
				<td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending"> {{$data['ordernum']}} </td>
				<td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending"> {{$data['date']}}</td>
				<td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending"> {{$data['amount']}}</td>
				<td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">{{$data['status']}}  </td>
                <td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">
                @if($data['status']=='Pending')
                <button class="btn btn-info btn-sm" style="padding: 5px 20px 5px 20px;"onclick="myFunction1()">Pay Now</button>  
                @elseif($data['status']=='Active')
                <button class="btn btn-success btn-md" style="padding: 5px 20px 5px 20px;"data-toggle="modal" data-target="#myModal">Invoice</button>
                @elseif($data['status']=='cancel')
                <button class="btn btn-primary btn-sm" style="padding: 5px 20px 5px 20px;"onclick="myFunction2()">Cancel</button>
                @endif
                </td>

               				
			</tr>
			@endforeach
			@endif
            </tbody>
                                      
                                       

                                       

<!-- {{print_r($orders['orders']['order'][0]['lineitems']['lineitem'][0]['billingcycle'])}} -->

            </table>
            </div>
            </div>
            </div>
            </div>
            </div>









            </div>
        </div>
    </div>

<div class="container">
  
  <!-- Trigger the modal with a button -->
  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
          <p>Your Invoice</p>
        </div>
       
      </div>
      
    </div>
  </div>
  
</div>



@endsection