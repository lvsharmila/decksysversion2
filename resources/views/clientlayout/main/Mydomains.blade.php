@extends('clientlayout.layouts.master')
@section('title')
Decksys | My Domains
@endsection

@section('content')


<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
        <div class="bg-light lter b-b wrapper-md">
            <h1 class="m-n font-thin h1 block">My Domains</h1>
        </div>
        <!-- Button trigger modal -->
        <div class="wrapper-md">
            <div class="panel panel-default">
                <div class="table-responsive">
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="row">
                            <div class="col-sm-12">                                
                            <table class="table table-striped m-b-none" style="text-align: center;" data-sort-order="desc">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending"aria-sort="descending">S.No</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Browser: activate to sort column ascending">Domain Name</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Browser: activate to sort column ascending">Registration Date</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Platform(s): activate to sort column ascending">Expiry Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Platform(s): activate to sort column ascending">Auto Renew</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Engine version: activate to sort column ascending">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($domain['domains']['domain'] as $key=> $value)
                                        @for($key=0;$key<100;$key++)
                                        @endfor
                                        <tr role="row">                                   
                                                                            
                                            <td class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0"rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending"aria-sort="descending"> {{$value['id']}} </td>
                                            <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Browser: activate to sort column ascending">{{$value['domainname']}}</td>
                                            <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['regdate']}}</td>
                                            <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['expirydate']}}</td>
                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                            <div class="onoffswitch center-block">
                                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                                                <label class="onoffswitch-label" for="myonoffswitch">
                                                <span class="onoffswitch-inner"></span>
                                                <span class="onoffswitch-switch"></span>
                                                </label>
                                            </div>
                                            </td> 
                                            <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1"
                                                colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                <div class="dropdown">
                                                    <button class="btn btn-primary dropdown-toggle center-block" type="button"
                                                        data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i>
                                                    </button>
                                                    <ul class="dropdown-menu">                                                        
                                                        <li><a href="" style="color:#23b7e5" data-toggle="modal"
                                                                data-target="#myModalD">Managed DNS</a></li>
                                                        <li><a href="" style="color:#23b7e5" data-toggle="modal"
                                                                data-target="#myModalDA">Contact Details</a></li>
                                                        <li><a href="#">Privacy Protection</a></li>
                                                        <li><a href="#">EPP Code</a></li>
                                                        <li><a href="#">Domain Forwarding</a></li>
                                                        <li><a href="#">Name Server</a></li>
                                                        <li><a href="#">Theft Protection</a></li>
                                                    </ul>
                                                </div>
                                            </td>                                           
                                        </tr>
                                        @endforeach
                                        <!-- Contact Details Start -->
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModalDA" tabindex="-1" role="dialog"
                                            aria-labelledby="myModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                    </div>
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        <div role="tabpanel">
                                                            <!-- Nav tabs -->
                                                            <ul class="nav nav-tabs nav-justified borderBottom" role="tablist">
                                                                <li role="presentation" class="active">
                                                                    <a class="nav-link active"href="#registerTab" aria-controls="registerTab" role="tab" data-toggle="tab">Registrant Contact
                                                                        <i class="fa fa-info-circle" aria-hidden="true" title="Details About Owner Or Registrant Of Domain- A Company Or An Individual "></i>
                                                                    </a>
                                                                </li>
                                                                <li role="presentation" class="nav-item">
                                                                    <a class="nav-link" href="#adminTab" aria-controls="adminTab" role="tab" data-toggle="tab">Administrative Contact
                                                                        <i class="fa fa-info-circle" aria-hidden="true" title="Details About Owner Or Registrant Of Domain- A Company Or An Individual"></i>
                                                                    </a>
                                                                </li>
                                                                <li role="presentation" class="nav-item">
                                                                    <a class="nav-link"href="#billingTab" aria-controls="billingTab"role="tab" data-toggle="tab">Billing Contact
                                                                        <i class="fa fa-info-circle" aria-hidden="true"title="Details Of Person Who'll Handle Billing Issues Of This Domain. Renewal Information Will Also Be Sent To Address Entered Here. Can Be Same As Registrant Contact "></i>
                                                                    </a>
                                                                </li>
                                                                <li role="presentation" class="nav-item">
                                                                    <a class="nav-link"href="#techTab" aria-controls="techTab" role="tab"data-toggle="tab">Technical Contact
                                                                    <i class="fa fa-info-circle"aria-hidden="true" title="Details Of Person Who'll Handle Issues Related To Web Hosting, Mail, DNS Etc.  Can Be Your Web Hosting Provider's Contact Information. "></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <!-- Tab panes Start -->
                                                            <div class="tab-content">
                                                                <!-- Registrant Contact Start -->
                                                                <div role="tabpanel" class="tab-pane active" id="registerTab">
                                                                    <h3>Registrant Contact </h3><br>
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tab1"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Name<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="name"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Organisation <span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="Organisation"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>City <span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="city"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Country<span class="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>India</option>
                                                                                                    <option>Switzer Land</option>
                                                                                                    <option>United States</option>
                                                                                                    <option>United Kingdom</option>
                                                                                                    <option>Norway</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Phone<span class="req"style="color:red">*</span></label>
                                                                                                <div class="input-group"style="width: 100%;">
                                                                                                    <span class="input-group-addon"id="sizing-addon1">+91</span>
                                                                                                    <input type="text"class="form-control"aria-describedby="sizing-addon1">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-check">
                                                                                                <input class="form-check-input"type="checkbox"value="" id="defaultCheck1">
                                                                                                <label class="form-check-label"for="defaultCheck1">
                                                                                                    Use same contact for Administrative, Billing & Technical details                                                                                                    
                                                                                                </label>
                                                                                            </div>
                                                                                            <div>
                                                                                                <button type="update" class="btn btn-primary">Update</button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tabl"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Address<span class="req"style="color:red">*</span></label>
                                                                                                <textarea class="form-control"rows="4"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label i>Email <span class="req"style="color:red">*</span></label>
                                                                                                <input type="email"name="Email" value=""class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Zip Code<span class="req" style="color:red">*</span></label>
                                                                                                <input type="text" name="ZipCode" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>State/Region/Province<span calss="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>Tamil Nadu</option>
                                                                                                    <option>Andhra Pradesh</option>
                                                                                                    <option>Karnataka</option>
                                                                                                    <option>Telangana</option>
                                                                                                    <option>Himachal Pradesh</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Registrant Contact End-->

                                                                <!-- Start Administrative Contact  -->
                                                                <div role="tabpanel" class="tab-pane" id="adminTab">
                                                                    <h3>Administrative Contact </h3><br>
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tab1" class="a_record_form" action="" method="POST">
                                                                                            <input type="hidden" name="_token" value="">
                                                                                            <div class="form-group">
                                                                                                <label>Name<span class="req" style="color:red">*</span></label>
                                                                                                <input type="text" name="name"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Organisation<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="Organisation"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>City <span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="city"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Country<span class="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>India</option>
                                                                                                    <option>Switzer Land</option>
                                                                                                    <option>United States</option>
                                                                                                    <option>United Kingdom</option>
                                                                                                    <option>Norway</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Phone<span class="req"style="color:red">*</span></label>
                                                                                                <div class="input-group"style="width: 100%;">
                                                                                                    <span class="input-group-addon"id="sizing-addon1">+91</span>
                                                                                                    <input type="text"class="form-control"aria-describedby="sizing-addon1">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-check">
                                                                                                <input class="form-check-input"type="checkbox"value="" id="defaultCheck1">
                                                                                                <label class="form-check-label"for="defaultCheck1">
                                                                                                    Use same contact for Administrative,Billing & Technical details
                                                                                                </label>
                                                                                            </div>
                                                                                            <div>
                                                                                                <button type="update"class="btn btn-primary">Update</button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tabl"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Address<span class="req"style="color:red">*</span></label>
                                                                                                <textarea class="form-control"rows="4"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label i>Email <span class="req"style="color:red">*</span></label>
                                                                                                <input type="email"name="Email" value=""class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Zip Code<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="ZipCode"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>State/Region/Province<span class="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>Tamil Nadu</option>
                                                                                                    <option>Andhra Pradesh</option>
                                                                                                    <option>Karnataka</option>
                                                                                                    <option>Telangana</option>
                                                                                                    <option>Himachal Pradesh</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Administrative Contact End -->


                                                                <!-- Tab OwnDomain panes Start -->
                                                                <div role="tabpanel" class="tab-pane" id="billingTab">
                                                                    <h3>Billing Contact </h3><br>
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tab1"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Name<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="name"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Organisation<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="Organisation"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>City <span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="city"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Country<span class="req" style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>India</option>
                                                                                                    <option>Switzer Land</option>
                                                                                                    <option>United States</option>
                                                                                                    <option>United Kingdom</option>
                                                                                                    <option>Norway</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Phone<span class="req"style="color:red">*</span></label>
                                                                                                <div class="input-group"style="width: 100%;">
                                                                                                    <span class="input-group-addon"id="sizing-addon1">+91</span>
                                                                                                    <input type="text"class="form-control"aria-describedby="sizing-addon1">
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-check">
                                                                                                <input class="form-check-input"type="checkbox"value="" id="defaultCheck1">
                                                                                                <label class="form-check-label"for="defaultCheck1">
                                                                                                    Use same contact for Administrative, Billing & Technical details</label>                                                                                                   
                                                                                            </div>
                                                                                            <div>
                                                                                                <button type="update"class="btn btn-primary">Update</button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tabl"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Address<span class="req" style="color:red">*</span></label>
                                                                                                <textarea class="form-control" rows="4"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label i>Email <span class="req"style="color:red">*</span></label>
                                                                                                <input type="email" name="Email" value=""class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Zip Code<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="ZipCode"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>State/Region/Province<span class="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>Tamil Nadu</option>
                                                                                                    <option>Andhra Pradesh</option>
                                                                                                    <option>Karnataka</option>
                                                                                                    <option>Telangana</option>
                                                                                                    <option>Himachal Pradesh</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Billing Contact End -->

                                                                <!-- Tab OwnDomain panes Start -->
                                                                <div role="tabpanel" class="tab-pane" id="techTab">
                                                                    <h3>Technical Contact </h3><br>
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tab1"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Name<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="name"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Organisation<span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="Organisation"value="" class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>City <span class="req"style="color:red">*</span></label>
                                                                                                <input type="text" name="city"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Country<span class="req"style="color:red">*</span></label>
                                                                                                <select class="form-control"id="exampleFormControlSelect1">
                                                                                                    <option>India</option>
                                                                                                    <option>Switzer Land</option>
                                                                                                    <option>United States</option>
                                                                                                    <option>United Kingdom</option>
                                                                                                    <option>Norway</option>
                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Phone<span class="req"style="color:red">*</span></label>
                                                                                                <div class="input-group"style="width: 100%;">
                                                                                                    <span class="input-group-addon" id="sizing-addon1">+91</span>
                                                                                                    <input type="text"class="form-control"aria-describedby="sizing-addon1">
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-check">
                                                                                                <input class="form-check-input"type="checkbox"value="" id="defaultCheck1">
                                                                                                <label class="form-check-label"for="defaultCheck1">
                                                                                                    Use same contact for Administrative, Billing & Technical details                                                                                                   
                                                                                                </label>
                                                                                            </div>
                                                                                            <div>
                                                                                                <button type="update"class="btn btn-primary">Update</button>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-body">
                                                                                        <form role="form" id="tabl"class="a_record_form"action="" method="POST">
                                                                                            <input type="hidden" name="_token"value="">
                                                                                            <div class="form-group">
                                                                                                <label>Address<span class="req"style="color:red">*</span></label>
                                                                                                <textarea class="form-control" rows="4"></textarea>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label i>Email <span class="req"style="color:red">*</span></label>
                                                                                                <input type="email"name="Email" value=""class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>Zip Code<span class="req" style="color:red">*</span></label>
                                                                                                <input type="text" name="ZipCode"class="form-control">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label>State/Region/Province<span class="req" style="color:red">*</span></label>
                                                                                                <select class="form-control" id="exampleFormControlSelect1">
                                                                                                    <option>Tamil Nadu</option>
                                                                                                    <option>Andhra Pradesh</option>
                                                                                                    <option>Karnataka</option>
                                                                                                    <option>Telangana</option>
                                                                                                    <option>Himachal Pradesh</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- Technical Contact End -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Details End -->
        </tbody>
        </table>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog-lg">
        <div class="modal-content1">
            <div class="modal-header">

                <h2 class="modal-title text-center" id="myModalLabel">DNS Management</h2>
            </div>

            <div class="modal-body">
                <div id="exTab1" class="container">
                    <ul class="nav nav-pills borderBottom">
                        <li class="active">
                            <a href="#1a" data-toggle="tab">A Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="Address Or A Records (Also Host Records) Are DNS's Central Records. They Link A Domain To An IP Address"></i></a>
                        </li>

                        <li>
                            <a href="#6a" data-toggle="tab">AAAA Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="Similar To A Record, An AAAA Record Points A Domain Or Subdomain To An IPv6 Address; But A Records Only Utilize IPv4 Addresses"></i></a>
                        </li>
                        <li><a href="#2a" data-toggle="tab">MX Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="MX Records Or Mail Exchange Records Are Basic Records For Directing Email For A Domain Name"></i></a>
                        </li>
                        <li><a href="#3a" data-toggle="tab">CNAME Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="CNAME Records Or Canonical Name Records Function As Aliases For Domain Names Of Other Canonical Domain Name."></i></a>
                        </li>
                        <li><a href="#4a" data-toggle="tab">NS Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="NS Records or Nameservers Enable DNS To Translate A Domain Name To An IP Address. For Ex, https://69.162.97.219 Resolves To Your Domain: https://www.yourdomain.com/. Also They Break A Domain Into Subdomains"></i></a>
                        </li>
                        <li><a href="#5a" data-toggle="tab">TXT Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="TXT Records Or Text Records Are Custom Records & Comprise Data Readable By Human Beings."></i></a>
                        </li>
                        <li><a href="#7a" data-toggle="tab">SRV Records <i class="fa fa-info-circle" aria-hidden="true"
                                    title="TXT Records Or Text Records Are Custom Records & Comprise Data Readable By Human Beings."></i></a>
                        </li>
                        <li><a href="#8a" data-toggle="tab">SOA Parameters <i class="fa fa-info-circle" aria-hidden="true"
                                    title="TXT Records Or Text Records Are Custom Records & Comprise Data Readable By Human Beings."></i></a>
                        </li>

                    </ul>

                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="1a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModala">A
                                Record</button>

                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr>
                                        <th>Host Name</th>
                                        <th>Destination IP Address</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>

                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModala"><i
                                                    class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                                data-target="#myModalA"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#myModalA"><i class="fa fa-trash"></i></button>
                                        </td>
                                     
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModalA" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content2">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title text-center"><strong>Edit
                                                                A Record</strong></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <form role="form" class="a_record_form" action=""method="POST">
                                                                            <input type="hidden" name="_token" value="">
                                                                            <div class="form-group">
                                                                                <label>Host Name</label>
                                                                                <input type="text" name="domainname"value="" class="form-control">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Current IPv4  Address<span calss="req" style="color:red">*</span></label>
                                                                                <input type="text" name="currentvalue"value="" class="form-control"readonly>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>New IPv4 Address
                                                                                    <span calss="req" style="color:red">*</span></label>
                                                                                <input type="text" name="newvalue" class="form-control">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>TTL<span calss="req" style="color:red">*</span></label>
                                                                                <input type="text" name="ttl" value=""class="form-control">
                                                                            </div>
                                                                            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </td>
                                            <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModala" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add A Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                        <form id="contact"role="form" class="a_record_form" method="post" action="">
			                                            <!-- Name -->
                                                            <div class="form-group">
                                                                <label class="text-inverse custom" for="contact_name">Host Name</label>
				                                                <input type="text" id="contact_name" name="name" class="form-control"></input>
				                                                <span class="error"> Enter Your Host Name</span>
			                                                </div>
			                                            <!-- Email -->
                                                         <div class="form-group">               
                                                            <label class="text-inverse custom" for="contact_email">Destination IPv4 Address<span class="req">*</span></label>
				                                            <input type="email" id="contact_email" name="email" class="form-control"></input>
				                                            <span class="error">Enter Your Destination IPv4 Address</span>				
			                                            </div>						
			                                            <!--Website -->
                                                        <div class="form-group">
				                                            <label class="text-inverse custom" for="contact_website">TTL</label>
				                                            <input type="url" id="contact_website" name="website" class="form-control" placeholder="38400"></input>
				                                            <span class="error"> Enter Your TTL</span>								
			                                            </div>						
			                                            <!-- Message -->								
			                                            <!-- Submit Button -->
			                                            <div id="contact_submit">				
                                                            <button type="submit" class="btn btn-sm btn-primary center-block" style="margin-right: 200px;">Submit</button>
			                                            </div>
		                                                </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="tab-pane" id="6a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalb"> AAAA Record</button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr>
                                        <th>Host Name</th>
                                        <th>Destination IP Address</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalb"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModalAA"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"data-target="#myModalAA"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <td>
                                            <!-- Modal -->
                                        <td>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModalAA" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit AAAA Record</strong></ h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form"action="" method="POST">
                                                                                <input type="hidden" name="_token"value="">
                                                                                <div class="form-group">
                                                                                    <label>Host Name</label>
                                                                                    <input type="text" name="domainname"class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Current IPv6 Address
                                                                                        <span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="currentvalue"value="" class="form-control"readonly>
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>New IPv6 Address <span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="newvalue"class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL<span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" value=""class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalb" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add AAAA Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                        <form id="contact1"role="form" class="a_record_form" method="post" action="">
			                                            <!-- Name -->
                                                            <div class="form-group">
                                                                <label class="text-inverse custom" for="contact_name1">Host Name</label>
				                                                <input type="text" id="contact_name1" name="name" class="form-control"></input>
				                                                <span class="error"> Enter Your Host Name</span>
			                                                </div>
			                                            <!-- Email -->
                                                         <div class="form-group">               
                                                            <label class="text-inverse custom" for="contact_email1">Destination IPv4 Address<span class="req">*</span></label>
				                                            <input type="email" id="contact_email1" name="email" class="form-control"></input>
				                                            <span class="error">Enter Your Destination IPv4 Address</span>				
			                                            </div>						
			                                            <!--Website -->
                                                        <div class="form-group">
				                                            <label class="text-inverse custom" for="contact_website1">TTL</label>
				                                            <input type="url" id="contact_website1" name="website" class="form-control" placeholder="38400"></input>
				                                            <span class="error"> Enter Your TTL</span>								
			                                            </div>						
			                                            <!-- Message -->								
			                                            <!-- Submit Button -->
			                                            <div id="contact_submit1">				
                                                            <button type="submit" class="btn btn-sm btn-primary center-block" style="margin-right: 200px;">Submit</button>
			                                            </div>
		                                                </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="2a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalc">MX Record</button>


                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Priority</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>5</td>

                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalc"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal2"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"data-target="#myModal2"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <!-- Modal -->
                                        <td>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal2" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit MX Record</strong></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form" action="" method="POST">
                                                                                <input type="hidden" name="_token"  value="{{csrf_token()}}">
                                                                                <div class="form-group">
                                                                                    <label>Host Name</label>
                                                                                    <input type="text" name="domainname" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Value <span class="req" style="color:red">*</span></label>
                                                                                    <input type="radio" value="notfqdn" name="dataportion">
                                                                                    <input type="text" name="value"value="" class="form-control">
                                                                                    <input type="radio" value="fqdn"checked name="dataportion">
                                                                                    <input type="text" name="newvaluefqdn"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Priority <span class="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="priority"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL <span class="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" value=""class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                               <!-- Modal -->
                                <div class="modal fade" id="myModalc" role="dialog">
                                    <div class="modal-dialog slideFromLeft">

                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add
                                                        MX Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">

                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">

                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Priority</label>
                                                                    <input type="text" name="priority" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="3a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModald"> CNAME Record</button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModald"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal3"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal3"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <!-- Modal -->
                                        <td>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal3" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit CNAME Record</strong></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form"action="" method="POST">
                                                                                <input type="hidden" name="_token"value="<?php echo csrf_token(); ?>">
                                                                                <div class="form-group">
                                                                                    <label>Zone</label>
                                                                                    <input type="text" name="domainname"class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Value <span class="req" style="color:red">*</span></label>
                                                                                    <input type="radio" value="notfqdn"checked="" name="dataportion">
                                                                                    <input type="text" name="value"value="" class="form-control">
                                                                                    <input type="radio" value="fqdn"name="dataportion">
                                                                                    <input type="text" name="valuefqdn"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL <span class="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                               </div>
                                        </td>
                                        <!-- Modal -->

                                    </tr>

                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->


                                <!-- Modal -->
                                <div class="modal fade" id="myModald" role="dialog">
                                    <div class="modal-dialog slideFromLeft">

                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add
                                                        CNAME Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">

                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">

                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="4a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModale">NS
                                Record</button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModale"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal4"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"data-target="#myModal4"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <!-- Modal -->
                                        <td>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal4" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit NS Record</strong></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form"action="" method="POST">
                                                                                <input type="hidden" name="_token"value="<?php echo csrf_token(); ?>">
                                                                                <div class="form-group">
                                                                                    <label>Zone</label>
                                                                                    <input type="text" name="domainname"class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Value<span calss="req" style="color:red">*</span></label>
                                                                                    <input type="radio" value="notfqdn"checked="" name="dataportion">
                                                                                    <input type="text" name="value"value="" class="form-control">
                                                                                    <input type="radio" value="fqdn"name="dataportion">
                                                                                    <input type="text" name="valuefqdn"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL<span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" value=""class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModale" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add
                                                        NS Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">

                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">

                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" value="" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="5a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalf">TXT
                                Record</button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalf"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal5"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"data-target="#myModal5"><i class="fa fa-trash"></i></button>
                                        </td>

                                        <div class="modal fade" id="myModal5" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content2">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title text-center"><strong>Edit TXT Record</strong></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <form role="form" class="a_record_form" action=""method="POST">
                                                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                            <div class="form-group">
                                                                                <label>Zone</label>
                                                                                <input type="text" name="domainname"class="form-control">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Value <span calss="req" style="color:red">*</span></label>
                                                                                <input type="radio" value="notfqdn"checked="" name="dataportion">
                                                                                <input type="text" name="value" value=""class="form-control">
                                                                                <input type="radio" value="fqdn" name="dataportion">
                                                                                <input type="text" name="valuefqdn"value="" class="form-control">
                                                                            </div>
                                                                           <div class="form-group">
                                                                                <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                                <input type="text" name="ttl" value=""class="form-control">
                                                                            </div>
                                                                            <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            </td>
                                            <!-- Modal -->
                                    </tr>

                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalf" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add TXT Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form" class="a_record_form">
                                                                <div class="form-group">
                                                                    <label>Host Name</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Destination IPv4
                                                                        Address <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="value" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalc" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add MX Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">
                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Priority</label>
                                                                    <input type="text" name="priority" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="7a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalg"> SRV
                                Record</button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>


                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalg"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal7"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"data-target="#myModal7"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <!-- Modal -->
                                        <td>
                                            <div class="modal fade" id="myModal7" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit SRV Record</strong></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form" action="" method="POST">
                                                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                                                <div class="form-group">
                                                                                    <label>Zone</label>
                                                                                    <input type="text" name="domainname" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                                    <input type="text" name="value" value="" class="form-control">
                                                                                    <input type="radio" value="fqdn"name="dataportion">
                                                                                    <input type="text" name="valuefqdn"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL<span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" value=""class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalg" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add SRV Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form" class="a_record_form">
                                                                <div class="form-group">
                                                                    <label>Host Name</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Destination IPv4 Address <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="value" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalc" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add MX Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">
                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Priority</label>
                                                                    <input type="text" name="priority" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- SOA Parameters -->
                        <div class="tab-pane" id="8a">
                            <button class="btn btn-info m-t-sm" data-toggle="modal" data-target="#myModalh"> SOA
                                Parameters
                            </button>
                            <table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }"
                                class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid"
                                aria-describedby="DataTables_Table_0_info">

                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>TTL</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalh"><i class="fa fa-plus"></i></button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"data-target="#myModal8"><i class="fa fa-edit"></i></button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal8"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <!-- Modal -->
                                        <td>
                                            <div class="modal fade" id="myModal8" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content2">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title text-center"><strong>Edit SOA Parameters</strong></h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-body">
                                                                            <form role="form" class="a_record_form"
                                                                                action="" method="POST">
                                                                                <input type="hidden" name="_token"value="<?php echo csrf_token(); ?>">
                                                                                <div class="form-group">
                                                                                    <label>Zone</label>
                                                                                    <input type="text" name="domainname"class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                                    <input type="text" name="value" value="" class="form-control">
                                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                                    <input type="text" name="valuefqdn"value="" class="form-control">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <label>TTL
                                                                                        <span calss="req" style="color:red">*</span></label>
                                                                                    <input type="text" name="ttl" value=""
                                                                                        class="form-control">
                                                                                </div>
                                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <!-- Modal -->
                                    </tr>
                                </tbody>
                            </table>
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                               <!-- Modal -->
                                <div class="modal fade" id="myModalh" role="dialog">
                                    <div class="modal-dialog slideFromLeft">
                                        <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add SOA Parameters</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form" class="a_record_form">
                                                                <div class="form-group">
                                                                    <label>Host Name</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Destination IPv4 Address <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="value" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModalc" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                        <div class="modal-content2">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title text-center"><strong>Add MX Record</strong></h4>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body">
                                                            <form role="form">
                                                                <div class="form-group">
                                                                    <label>Zone</label>
                                                                    <input type="text" name="domainname" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Value <span calss="req" style="color:red">*</span></label>
                                                                    <input type="radio" value="notfqdn" checked="" name="dataportion">
                                                                    <input type="text" name="value" value="" class="form-control">

                                                                    <input type="radio" value="fqdn" name="dataportion">
                                                                    <input type="text" name="valuefqdn" value="" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>TTL <span calss="req" style="color:red">*</span></label>
                                                                    <input type="text" name="ttl" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Priority</label>
                                                                    <input type="text" name="priority" class="form-control">
                                                                </div>
                                                                <button type="submit" class="btn btn-sm btn-primary center-block">Submit</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End SOA Parameters -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
  @endsection
