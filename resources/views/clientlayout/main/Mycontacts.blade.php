@extends('clientlayout.layouts.master')

@section('title')
Decksys | My Contacts
@endsection
@section('content')

<div id="content" class="app-content" role="main">
  	    <div class="app-content-body ">   
            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h1 block">My Contacts</h1>
            </div>      
 <!-- Button trigger modal -->

<!-- Registrant Contact Start -->
 <h3 style="text-align:center">Registrant Contact </h3>

                <div class="wrapper-md">
                    <div class="panel panel-default"> 
                    
                        <div class="table-responsive">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table ui-jq="dataTable" ui-options="{
                                            sAjaxSource: 'api/datatable.json',
                                            aoColumns: [
                                                { mData: 'engine' },
                                                { mData: 'browser' },
                                                { mData: 'platform' },
                                                { mData: 'version' },
                                                { mData: 'grade' }
                                            ]
                                            }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        
                                                <thead>
                                                <tr role="row">
                                                <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">First Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Last Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Email</th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Phone Number </th>                                                
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Add New </th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
                                                </tr>
                                                </thead>
                                                
                                                    <tbody>
                                                    
                                                        <tr role="row">
                                                            <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            
                                                            </td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModala"><i
                                                    class="fa fa-plus"></i></button>
                                                            </td>   
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending"> 
                                                                    <div class="dropdown">
                                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                       
                                                                           
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal">DNS</a></li>
                                                                            
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal1">Login Details</a></li>
                                                                        </ul>
                                                                </div></div>
                                                            </td>  
    
                                                        </tr>
                                                        
                                                        <!--DNS Modal Start-->
                                                        <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">DNS Records</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row" style="padding-top:20px;">
                                                                        <div class="col-lg-6">
                                                                            <h4 >Current DNS</h4>
                                                                            <a href="" class="block panel padder-v bg-primary item" >
                                                                                <span class="text-white font-thin h1 block text-center"></span>
                                                                                <p class="text-center text-muted customFont" > Values - from cpanel</p>
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-lg-6"  style="padding-left:20px;">
                                                                            <h4 >Default DNS</h4>    
                                                                                <a href="" class="block panel padder-v bg-info item">
                                                                                    <span class="text-white font-thin h1 block text-center"></span>
                                                                                    <p class="text-muted text-center customFont">Decksys Default DNS Record </p>
                                                                                </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    <!-- DNS Modal End -->

                                                    <!--Login Modal Start-->
                                            <div class="modal fade" id="myModal1" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><b>Information about Cpanel Login</b></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <label for="uname"><b>Username:</b></label>
                                                        </div>
                                                        <div>                                            
                                                            <label for="psw"><b>Password:</b></label>
                                                        </div>
                                                        <a href=""><b>URL to login:</b></a>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <!--Login modal End  -->

                                                    </tbody>
                                                    
                                            </table>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        
    
                    </div>  
	            </div>
                <!-- Registrant Contact End -->

                <!-- Administrative Contact Start -->
                <h3 style="text-align:center">Administrative Contact</h3>
                <div class="wrapper-md">
                    <div class="panel panel-default"> 
                    
                        <div class="table-responsive">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table ui-jq="dataTable" ui-options="{
                                            sAjaxSource: 'api/datatable.json',
                                            aoColumns: [
                                                { mData: 'engine' },
                                                { mData: 'browser' },
                                                { mData: 'platform' },
                                                { mData: 'version' },
                                                { mData: 'grade' }
                                            ]
                                            }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        
                                                <thead>
                                                <tr role="row">
                                                <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">First Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Last Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Email</th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Phone Number </th>                                                
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Add New </th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
                                                </tr>
                                                </thead>
                                                
                                                    <tbody>
                                                    
                                                    <tr role="row">
                                                            <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            
                                                            </td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModala"><i
                                                    class="fa fa-plus"></i></button>
                                                            </td>   
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending"> 
                                                                    <div class="dropdown">
                                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                       
                                                                           
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal">DNS</a></li>
                                                                            
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal1">Login Details</a></li>
                                                                        </ul>
                                                                </div></div>
                                                            </td>  
    
                                                        </tr>
                                                        
                                                        <!--DNS Modal Start-->
                                                        <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">DNS Records</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row" style="padding-top:20px;">
                                                                        <div class="col-lg-6">
                                                                            <h4 >Current DNS</h4>
                                                                            <a href="" class="block panel padder-v bg-primary item" >
                                                                                <span class="text-white font-thin h1 block text-center"></span>
                                                                                <p class="text-center text-muted customFont" > Values - from cpanel</p>
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-lg-6"  style="padding-left:20px;">
                                                                            <h4 >Default DNS</h4>    
                                                                                <a href="" class="block panel padder-v bg-info item">
                                                                                    <span class="text-white font-thin h1 block text-center"></span>
                                                                                    <p class="text-muted text-center customFont">Decksys Default DNS Record </p>
                                                                                </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    <!-- DNS Modal End -->

                                                    <!--Login Modal Start-->
                                            <div class="modal fade" id="myModal1" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><b>Information about Cpanel Login</b></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <label for="uname"><b>Username:</b></label>
                                                        </div>
                                                        <div>                                            
                                                            <label for="psw"><b>Password:</b></label>
                                                        </div>
                                                        <a href=""><b>URL to login:</b></a>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <!--Login modal End  -->

                                                    </tbody>
                                                    
                                            </table>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        
    
                    </div>  
	            </div>
                <!-- Administrative Contact End -->

                <!-- Billing Contact Start -->
                <h3 style="text-align:center">Billing Contact</h3>
                <div class="wrapper-md">
                    <div class="panel panel-default"> 
                    
                        <div class="table-responsive">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table ui-jq="dataTable" ui-options="{
                                            sAjaxSource: 'api/datatable.json',
                                            aoColumns: [
                                                { mData: 'engine' },
                                                { mData: 'browser' },
                                                { mData: 'platform' },
                                                { mData: 'version' },
                                                { mData: 'grade' }
                                            ]
                                            }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        
                                                <thead>
                                                <tr role="row">
                                                <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">First Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Last Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Email</th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Phone Number </th>                                                
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Add New </th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
                                                </tr>
                                                </thead>
                                                
                                                    <tbody>
                                                    
                                                    <tr role="row">
                                                            <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            
                                                            </td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModala"><i
                                                    class="fa fa-plus"></i></button>
                                                            </td>    
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending"> 
                                                                    <div class="dropdown">
                                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                       
                                                                           
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal">DNS</a></li>
                                                                            
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal1">Login Details</a></li>
                                                                        </ul>
                                                                </div></div>
                                                            </td>  
    
                                                        </tr>
                                                        
                                                        <!--DNS Modal Start-->
                                                        <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">DNS Records</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row" style="padding-top:20px;">
                                                                        <div class="col-lg-6">
                                                                            <h4 >Current DNS</h4>
                                                                            <a href="" class="block panel padder-v bg-primary item" >
                                                                                <span class="text-white font-thin h1 block text-center"></span>
                                                                                <p class="text-center text-muted customFont" > Values - from cpanel</p>
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-lg-6"  style="padding-left:20px;">
                                                                            <h4 >Default DNS</h4>    
                                                                                <a href="" class="block panel padder-v bg-info item">
                                                                                    <span class="text-white font-thin h1 block text-center"></span>
                                                                                    <p class="text-muted text-center customFont">Decksys Default DNS Record </p>
                                                                                </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    <!-- DNS Modal End -->

                                                    <!--Login Modal Start-->
                                            <div class="modal fade" id="myModal1" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><b>Information about Cpanel Login</b></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <label for="uname"><b>Username:</b></label>
                                                        </div>
                                                        <div>                                            
                                                            <label for="psw"><b>Password:</b></label>
                                                        </div>
                                                        <a href=""><b>URL to login:</b></a>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <!--Login modal End  -->

                                                    </tbody>
                                                    
                                            </table>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        
    
                    </div>  
	            </div>
                <!-- Billing Contact End -->

                <!-- Technical Contact Start -->
                <h3 style="text-align:center">Technical Contact</h3>
                <div class="wrapper-md">
                    <div class="panel panel-default"> 
                    
                        <div class="table-responsive">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table ui-jq="dataTable" ui-options="{
                                            sAjaxSource: 'api/datatable.json',
                                            aoColumns: [
                                                { mData: 'engine' },
                                                { mData: 'browser' },
                                                { mData: 'platform' },
                                                { mData: 'version' },
                                                { mData: 'grade' }
                                            ]
                                            }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        
                                                <thead>
                                                <tr role="row">
                                                <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">First Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Last Name</th>
                                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Email</th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Phone Number </th>                                                
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Add New </th>
                                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Action </th>
                                                </tr>
                                                </thead>
                                                
                                                    <tbody>
                                                    
                                                    <tr role="row">
                                                            <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td>
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending"></td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            
                                                            </td> 
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
                                                            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModala"><i
                                                    class="fa fa-plus"></i></button>
                                                            </td>   
                                                            <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending"> 
                                                                    <div class="dropdown">
                                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                                                        <ul class="dropdown-menu">
                                                                       
                                                                           
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal">DNS</a></li>
                                                                            
                                                                            <li><a href="#" data-toggle="modal" data-target="#myModal1">Login Details</a></li>
                                                                        </ul>
                                                                </div></div>
                                                            </td>  
    
                                                        </tr>
                                                        
                                                        <!--DNS Modal Start-->
                                                        <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog">
                                                            <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">DNS Records</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="row" style="padding-top:20px;">
                                                                        <div class="col-lg-6">
                                                                            <h4 >Current DNS</h4>
                                                                            <a href="" class="block panel padder-v bg-primary item" >
                                                                                <span class="text-white font-thin h1 block text-center"></span>
                                                                                <p class="text-center text-muted customFont" > Values - from cpanel</p>
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-lg-6"  style="padding-left:20px;">
                                                                            <h4 >Default DNS</h4>    
                                                                                <a href="" class="block panel padder-v bg-info item">
                                                                                    <span class="text-white font-thin h1 block text-center"></span>
                                                                                    <p class="text-muted text-center customFont">Decksys Default DNS Record </p>
                                                                                </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             </div>
                                                        </div>
                                                    </div>
                                                    <!-- DNS Modal End -->

                                                    <!--Login Modal Start-->
                                            <div class="modal fade" id="myModal1" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"><b>Information about Cpanel Login</b></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div>
                                                            <label for="uname"><b>Username:</b></label>
                                                        </div>
                                                        <div>                                            
                                                            <label for="psw"><b>Password:</b></label>
                                                        </div>
                                                        <a href=""><b>URL to login:</b></a>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                        <!--Login modal End  -->

                                                    </tbody>
                                                    
                                            </table>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        
    
                    </div>  
	            </div>
                <!-- Technical Contact End -->

@endsection