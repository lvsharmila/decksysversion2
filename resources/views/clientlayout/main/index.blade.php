@extends('clientlayout.layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')

    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">

<?php //echo"<pre>";print_r($response);exit;?>
            <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
    app.settings.asideFolded = false; 
    app.settings.asideDock = false;">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">Dashboard</h1>

                            </div>

                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
                        <!-- stats -->

                        <!-- / stats -->
                        <!-- frist row Starts-->
                        <div class="row">
                            <div class="col-lg-3">
                      
                                <div class="panel bg-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-globe fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9">                                              

                                             <div style="font-size:30px; color:#fff;" >Services</div>                                             </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-lg-3">
                           
                                <div class="panel bg-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-wrench fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">                                             

                                                <div style="font-size:30px; color:#fff;">Domains</div> 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3">
                           
                                <div class="panel bg-info">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-wrench fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">                                             

                                                <div style="font-size:30px; color:#fff;">Tickets</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3">
                           
                                <div class="panel bg-dark">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-edit fa-5x "></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge" style="font-size:45px; color:#fff;">  </div>
                                                <div style="font-size:30px; color:#fff;">Accounts</div> 
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>



<!-- Services Section Start-->

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="panel panel-default bg-light">
                                    <div class="panel-heading text-center" style="background-color: #bababa;">
                                        <h4 class="font-thin m-t-none m-b-none text-white text-center">Services</h4>
                                    </div>
					                    <div class="table-responsive">
                                            <table class="table table-striped m-b-none" style="text-align: center;" data-sort-order="desc">
                                            <thead>
                                            <tr>
                                            <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Name</th>
                                            <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Reg Date</th>
                                            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Next DueDate</th>
                                            <!-- <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Actions</th> -->
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($data['totalresults'] == '0')
                                                <td> - </td>
                                                <td> - </td>
                                                <td> - </td>
                                                <td> - </td>
                                                <td> - </td>
                                                <td> - </td>
                                            @else
                                              @foreach($data['products']['product'] as $key)
                                                <tr role="row">
                                                  <!-- <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">{{$key['id']}}</td> -->
                                                  <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">          
                                                  <!--<a href="" style="color:#23b7e5" data-toggle="modal" data-target="#myModal">-->{{$key['id']}}</td>
                                                  <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['groupname']}} - {{$key['name']}}</td>
                                                  <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['regdate']}}</td>
                                                  <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$key['nextduedate']}}</td>
                                                  <!-- <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$key['status']}}</td> -->
                                                  
                                                </tr>         
                                              @endforeach
                                          @endif
                                          </tbody>
                                            </table>
				                        </div>
                                </div>
                            </div>
                        
                        <!-- Services Section End-->

                        <!-- Invoices Section Start-->
 <div class="col-lg-6">
                        <div class="panel panel-default bg-light">
                        <div class="panel-heading text-center" style="background-color: #bababa;">
                            <h4 class="font-thin m-t-none m-b-none text-white text-center">Services</h4>
                        </div>
                            <div class="table-responsive">
                                <table class="table table-striped m-b-none" style="text-align: center;" data-sort-order="desc">
                                <thead>
                                <tr>
                                <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">InvoiceDate</th>
                                <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Total</th>
                                <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Status</th>
                               
                                </tr>
                                </thead>
                                <tbody>
                                @if($response['totalresults'] == '0')
                                    <td> - </td>
                                    <td> - </td>
                                    <td> - </td>
                                    <td> - </td>
                                    <td> - </td>
                                    <td> - </td>
                                @else
                                @foreach($response['invoices']['invoice'] as $key=>$value)
                          @for($key=0;$key<100;$key++)
                          @endfor
                                    <tr role="row">
                                      <!-- <td  class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">{{$key['id']}}</td> -->
                                      <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">          
                                      <!--<a href="" style="color:#23b7e5" data-toggle="modal" data-target="#myModal">-->{{$value['id']}}</td>
                                      <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['datepaid']}}</td>
                                      <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['subtotal']}}</td>
                                      <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['status']}}</td>
 
                                      
                                    </tr>         
                                  @endforeach
                              @endif
                              </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                
                        </div>
                        <!-- Invoivces Section End-->

                       
    <!-- /content -->
@endsection
