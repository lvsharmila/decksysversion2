@extends('clientlayout.layouts.master')

@section('title')
Decksys | Change Password
@endsection
@section('content')

<div id="content" class="app-content" role="main">
  	    <div class="app-content-body ">   
            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h1 block">Change Password</h1>
            </div>      

            <div class="container">
	<div class="row">
        
		<div class="col-md-6 col-md-offset-3 form-area ">
		    
		    <label>Current Password</label>
		    <div class="form-group pass_show"> 
                <input type="password" value="" class="form-control" placeholder="Current Password"> 
            </div> 
		       <label>New Password</label>
            <div class="form-group pass_show"> 
                <input type="password" value="" class="form-control" placeholder="New Password"> 
            </div> 
		       <label>Confirm Password</label>
            <div class="form-group pass_show"> 
                <input type="password" value="" class="form-control" placeholder="Confirm Password"> 
            </div> 
            <div class="row">
                        <div class="text-center" >
                            <button class="btn btn-info" type="submit">Submit</button>
                        </div>
                    </div>
		</div>  
	</div>
</div>


@endsection