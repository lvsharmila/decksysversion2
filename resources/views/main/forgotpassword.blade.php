@extends('layouts.master12')
@section('title')
    Register
@endsection
@section('content')
<div class="container" style="max-width: 100% !important;background-color: #fff;">    
    <div class="row pb-5">
    <div class="offset-lg-2 col-lg-8 col-sm-12 p-5 border rounded main-section"style="margin-top:55px; background-color: ghostwhite;">
    
        <?php if(isset($_GET['error'])){ ?>
        <p class="text-center text-danger">Email ID Not Register </p>
        <?php } ?>
        <?php if(isset($_GET['success'])){ ?>
        <p class="text-center text-success">Please Verify Your Email ID</p>
        <?php } ?>
        <form class="container" id="needs-validation" action="{{route('login.forgotpassword_action')}}" method="post" novalidate>
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
          <p style="text-align:center;">Enter your email address below to begin the reset process.</p>
          <div class="row">
            <div class="offset-lg-3 col-lg-6 col-sm-6">
               <div class="form-group">
                <label class="text-inverse " for="validationCustom01">Email<span class="req">*</span></label>
                <input type="Email" name="email" class="form-control" id="validationCustom01" placeholder="Email" value="" maxlength="100">
                <div class="invalid-feedback">
                  Enter Your Email
                </div>
              </div>
            </div></div>  
          
          <div class="row">
            
            <div class="col-lg-12 col-sm-6 col-12"  style="margin-left: 200px;">
              <div class="form-group">
              
                  
                
            
              </div>  
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-12 text-center" style="
    padding-bottom: 20px;
">
                <button class="btn btn-info" type="submit">Submit</button>
            </div>
          </div>  
        </form>
      </div>
    </div>  
</div>
@endsection