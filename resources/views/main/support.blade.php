@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')


<section class="supportSection1 pt-4 pb-4" >
<div class="text-center p-3">
    <h2><b>SUPPORT FROM DECKSYS	</b></h2>
</div>
<div class="container-fluid">
    <div class="row text-center">
        <div class="col-12 col-sm-12 col-md-6  col-lg-4 pb-3">
            <div class="card cards pb-5">
                <div class="card-body">
                    <h4 class="card-title"><strong>SEARCH OUR <br>KNOWLEDGEBASE</strong></h4>
                    <p class="card-text">Hundreds of articles written with the purpose of answering your questions.</p>
                    <a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-search" aria-hidden="true"></i> SEARCH NOW</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
            <div class="card cards pb-5" >
                <div class="card-body">
                    <h4 class="card-title"><strong>EMAIL A DECKSYS <br> DEPARTMENT</strong></h4>
                    <p class="card-text">Get the help you need by sending an email through our simple online form.</p>
                    <a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-envelope" aria-hidden="true"></i> SUBMIT A REQUEST</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
            <div class="card cards pb-5">
                <div class="card-body">
                    <h4 class="card-title"><strong>CHAT WITH A <br> SUPPORT AGENT</strong></h4>
                    <p class="card-text">Our trained staff are online 24/7 and ready to assist you in a chat session.</p>
                    <a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-comments-o" aria-hidden="true"></i> START A CHAT</a>
                </div>
            </div>
        </div>
        <div>
            <p class="p-4 ml-5 text-center">At <strong style="color:#29539e;">Decksys </strong> we take support very seriously. Whether your question is purely technical or about the day-to-day management of your account, we have it covered.
                <br> In a rush? Why not try our FAQ where you can find quick and easy answers. If your question is unique, then our 24/7 support team are at hand.</p>
        </div>
    </div>
</div>
</section>
<section class="supportSection2" >
<div class="container-fluid">
    <div class="section2-body text-center p-5 col-12 col-sm-12 col-md-12 col-lg-12">
        <h2><strong>SEARCH THE KNOWLEDGEBASE</strong></h2>
        <p>We have an extensive <strong style="color:#29539e;">Decksys </strong>Knowledgebase full of useful articles and how-to guides.
            If you need guidance on a specific task, we recommend that you first search our knowledgebase with the search box below before contacting support.</p>
        <div class="input-group mb-3 p-4">
            <input class="form-control input-lg" placeholder="Search the Knowledgebase..." id="inputlg" type="text">
            <div class="input-group-append">
                <button type="submit" class="btn btn-md btn-success">Search <i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</div>
</section>
<section class="supportSection3 pt-4 pb-4" >
<div class="container-fluid">
    <h2 class="text-center"><b>SEND US AN EMAIL</b></h2>
    <p>We are always happy to hear from our customers. Whether you have a simple question or a more complex matter you would like to discuss, our support team at <strong style="color:#29539e;">decksys.com </strong> is always available.
         Fill out the form below to get in touch.</p>		
    <div class="section3-body text-center p-5">			
        <div class="row no-gutters text-left ">
            <div class="form-part1 col-12  col-sm-12 col-md-6 col-lg-6">
                <form >
                    <div>
                        <label for="usr">DEPARTMENT:</label>
                        <select class="form-control" style="width:95%;" id="sel1">
                            <option>Select A Department</option>
                            <option>Sales</option>
                            <option>Support</option>
                            <option></option>
                        </select>
                    </div>
                    <div>
                        <label for="pwd">EMAIL ADDRESS:</label>
                        <input type="text" class="form-control" placeholder="" name="name" style="width:95%;">
                    </div>
                    <div>
                        <label for="pwd">WEBSITE:</label>
                        <input type="text" class="form-control" placeholder="" name="name" style="width:95%;">
                    </div>
                </form>
            </div>
            <div class="form-part2 col-sm-12 col-md-6 col-lg-6">
                <form >
                    <div>
                        <label for="pwd">NAME:</label>
                        <input type="text" class="form-control" placeholder="" name="name" style="width:95%;">
                    </div>
                    <div>
                        <label for="pwd">PHONE:</label>
                        <input type="text" class="form-control" placeholder="" name="name" style="width:95%;">
                    </div>
                    <div class="pt-5">
                        <div class="btn  btn-sm float-left">
                            <input type="file" style="width:100%;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="form-part3 col-sm-12 col-md-12 col-lg-12">
                <form >
                    <div>
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="7" id="comment" style="width:97%;"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>
<section class="supportSection4 pt-4 pb-4">
<div class="container-fluid pr-5">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-3 text-center">
            <img src="/img/ico/download.png" alt="download">
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-6 text-center">
            <h1>DOWNLOAD OUR WHITE PAPER</h1>
            <p><strong>Your Guide To Basic Server Security:</strong> An overview of the techniques and methods you can use to secure your server and help keep it protected from threats.</p>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-3 text-center">
            <a href="#" class="downloadButton btn btn-lg btn-success"> DOWNLOAD WHITE PAPER</a>
        </div>
    </div>
</div>
</section>

@endsection
