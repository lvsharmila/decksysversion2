@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')

<?php //echo "<pre>";print_r($products);exit;?>

<!-- @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")                               
                                @if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
                                    @foreach($value['pricing'] as $key)                                       
                                             
                                                @foreach($value['configoptions']['configoption'] as $configure)                                  
                                                    @if($configure['name']=='Server Addons - OS')
                                                        @foreach($configure['options']['option'] as $key=>$options)                                      
                                                            @if($options['name'] == 'Windows Server')
                                                                {{$options['pricing']['INR']['monthly']}}
																													  @endif
                                                        @endforeach
																										@elseif($configure['name']=='Server Addons - Database')
																										@foreach($configure['options']['option'] as $key=>$options)
																												                                       
                                                            @if($options['name'] == 'Microsoft SQL Server')
                                                                {{$options['pricing']['INR']['monthly']}}
																													  @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach           
                                    @endforeach    
                                @endif
                            @endif
                        @endforeach -->

<div class="container-fluid  text-center mb-0">
<div class="row dedicatedServer">
	 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<h2>BENEFITS WITH OUR DEDICATED SERVER :<strong style="color:#29539e;"> Affordable, Fast,
			<br> Secure, Complete </strong></h2>
	 </div>
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 dedicatedServer"> 
	 <div class="table-responsive">
	 <form>
		<table class="table">
		  <thead>
			<tr>
			  <th>PROCESSOR</th>
			  <th>RAM</th>
			  <th>BANDWIDTH/Mo</th>
			  <th>STORAGE</th>
			  <th>IP ADDRESS</th>
			  <th>OPERATING SYSTEM</th>
			  <th>DATABASE</th>
			  <th>PRICE</th>
			  <th>ORDER</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td>Xeon E3 1230v3 Quad Core Server</td>
			  <td>8GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>
			  <td>  
					
						<div class="form-group">
							 <select class="form-control" id="selectOS">
								<option value=''>Select OS</option>
								<option value='value1'>CentOS 6</option>
								<option value='value2'>Ubuntu </option>
								<option value='value3'>Windows 2012</option>
							  </select>
						 </div>
					
				  </td>				  
				   <td>
					   
						<div class="form-group">
							  <select class="form-control" id="selectDB1">
								<option value=''>Select Database</option>
								<!-- <option value='No Cost'>MYSQL</option> -->

								@foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")                               
                                @if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
                                    @foreach($value['pricing'] as $key)                                       
                                             
                                                @foreach($value['configoptions']['configoption'] as $configure)                                  
                                                    @if($configure['name']=='Server Addons - OS')
                                                        @foreach($configure['options']['option'] as $key=>$options)                                      
                                                            @if($options['name'] == 'CentOS 6')
                                                                {{$options['pricing']['INR']['monthly']}}
																													  @endif
                                                        @endforeach
																										@elseif($configure['name']=='Server Addons - Database')
																										@foreach($configure['options']['option'] as $key=>$options)
																										@if($options['name'] == 'MySQL')
																										<option value='No Cost'>MYSQL</option>                                   
                                                            @elseif($options['name'] == 'Microsoft SQL Server')
																														<option value='{{$var=$options['pricing']['INR']['monthly']}}'>MSSQL </option>
																																
																													  @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach           
                                    @endforeach    
                                @endif
                            @endif
                        @endforeach
							
								<!-- <option value='10,000'>MSSQL </option> -->
							   </select>
						</div>
					  
										  </td>
			  <td style="font-size:20px;"><sup> <i class="fa fa-inr"></i>
				
@foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")
                               
                                @if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
																		{{$key['monthly']+$var}}
																		@endforeach
																		
																@endif
														@endif
											@endforeach
				 </sup> <sub id="db1"></sub></td>
			  <td class="text-center"><button type="button" class="btn btn-md btn-success">ORDER NOW</button></td>
			</tr>
			<tr>
			  <td>Xeon E3 1240v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>
			  <td>  
				  
						<div class="form-group">
							  <select class="form-control" id="selectOS">
								<option value=''>Select OS</option>
								<option value='value1'>CentOS 6</option>
								<option value='value2'>Ubuntu </option>
								<option value='value3'>Windows 2012</option>
							   </select>
						</div>
					
				</td>
				   <td>
										<div class="form-group">
							  <select class="form-control" id="selectDB2">
								<option value=''>Select Database</option>
								<option value='No Cost'>MYSQL</option>
								<option value='12,000'>MSSQL </option>
							   </select>
						</div>
					  
				  </td>
			  <td style="font-size:20px;"><sup> <i class="fa fa-inr"></i> @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")
                               
                                @if($value['name'] == "Xeon E3 1240v5 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
																		{{$key['monthly']}}
																		@endforeach
																		
																@endif
														@endif
											@endforeach </sup> <sub id="db2"></sub></td>
			  <td class="text-center"><button type="button" class="btn btn-md btn-success">ORDER NOW</button></td>
			</tr>
			<tr>
			  <td>Xeon E3 1270v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>
			  <td>  
				 
						<div class="form-group">
							  <select class="form-control" id="selectOS">
								<option value=''>Select OS</option>
								<option value='value1'>CentOS 6</option>
								<option value='value2'>Ubuntu </option>
								<option value='1000'>Windows 2012</option>
							   </select>
						</div>
				
				</td>
				<td>
										<div class="form-group">
							 <select class="form-control" id="selectDB3">
								<option value=''>Select Database</option>
								<option value='No Cost'>MYSQL</option>
							
								<option value='14,000'>MSSQL </option>
							  </select>
						</div>
				
				  </td>
			  <td style="font-size:20px;"><sup> <i class="fa fa-inr"></i> 
				@foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "6")
                               
                                @if($value['name'] == "Xeon E3 1270v5 Quad Core Server")
                                    @foreach($value['pricing'] as $key)
																		{{$key['monthly']}}
																		@endforeach
																		
																@endif
														@endif
											@endforeach
				</sup> <sub id="db3"></sub></td>
			  <td class="text-center"><button type="button" class="btn btn-md btn-success">ORDER NOW</button></td>
			</tr>
		  </tbody>
		</table>
		</form>
	</div>
	</div>
	
  </div>
</div><!-- Dedicated Server  : Table End-->
<script>
$('#selectDB1').on('change', function() {
var value = $(this).val(); 
$("#db1").show("value");
document.getElementById("db1").innerHTML= value;   
});$('#selectDB2').on('change', function() {
var value = $(this).val(); 
$("#db2").show("value");
document.getElementById("db2").innerHTML= value;   
});$('#selectDB3').on('change', function() {
var value = $(this).val(); 
$("#db3").show("value");
document.getElementById("db3").innerHTML= value;   
});
</script>	

<!-- Dedicated Server Features Section-->	
<section class="dedicated_features">
<div class="container text-center">
  <h2><b>FEATURES</b></h2>         
	  <table class="table table-bordered " style="box-shadow:20px 20px 40px grey;">
		<thead>
		  <tr>
			<th style="font-size:19px;background-color: #07539e;color:white;"><b>Features</b></th>
			<th class="bg-dark text-white" style="font-size:19px;">Dedicated Server</th>
		  </tr>
		</thead>
		<tbody>
		  <tr>
			<td class="features">Web site hosting</td>
			<td><strong>$10</strong></td>
		  </tr>
		  <tr>
			<td class="features">Root Access</td>
			<td><img  src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features1"></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">24x7 Support</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Optional Support</td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}} " height="30px" alt="dedicated_features2" ></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">Free SSL</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Dedicated hosting</td>
			<td><strong>$100</strong></td>
		  </tr>			  </tr>
		  <tr>
			<td class="features">Free Website Migration
				<a href="#" data-toggle="tooltip" title="Free website migration is available on Dedicated Server purchased for a minimum 3 months tenure." style="color: #337ab7;
				 text-decoration: none;cursor:point;background-color: transparent;"><span class="glyphicon glyphicon-info-sign"></span>
				 <img src="{{asset('img/ico/Information.png')}}" alt="Information.png" >
				 </a></td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features2" ></td>
		  </tr>
		</tbody>
	  </table>
</div>
</section>
@endsection
