@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')



<section class="dedicated">
<div class="container-fluid p-4">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
             <div class="thumbnail boxShadow">
                <div class="caption text-center">
                    <h4 class="p-4 background  text-white">DEDICATED SERVER</h4>
                    <ul class= "list"> 
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Free SSL security</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> 24x7x365 support team</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> 99.9% SLA1</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Root access</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Super fast, high reliability, Solid State Disks</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Global Data Centers</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="thumbnail boxShadow">
                <div class="caption text-center">
                    <h4 class="p-4 background  text-white">VPS</h4>
                    <ul class= "list"> 
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Free SSL security</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> 24x7x365 support team</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> 99.9% SLA1</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Root access</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Super fast, high reliability, Solid State Disks</li>
                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i> Global Data Centers</li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- End row -->
</div><!-- End container -->
</section>

<section class="services">
<div class="container-fluid serv">
    <h2 class="p-4  text-center "><b>FEATURES</b></h2>
    <div class="row no-gutters">
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                        <img src="{{asset('img/Home/dd-hosting.png')}}" alt="Ded_Hos">
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">Dedicated Server</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                    <img src="{{asset('img/Home/cloud-hosting.png')}}" alt="Clou_Hos">
                      
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">Cloud Hosting</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                    <img src="{{asset('img/Home/cloud-vps.png')}}" alt="Cloud_VPS">
                      
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">Cloud VPS</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                    <img src="{{asset('img/Home/sme-hosting.png')}}" alt="Sha_Hos">
                      
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">Shared / SME Hosting</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                    <img src="{{asset('img/Home/wp-hosting.svg')}}" alt="Mag_WP">
                        
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">Managed Wordpress</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="serviceHolder">
                <div class="servicesContent">
                    <div class="imgContainer">
                    <img src="{{asset('img/Home/vps-hosting.png')}}" alt="VPS_Hos">
                     
                    </div>
                    <div class="textContainer">
                        <p class="text-dark">VPS Hosting</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<section class="FAQ">
<h2 class="text-center p-4 "><b>FAQ</b></h2>
<div class="container-fluid">
    <div class="col-md- p-4">
        <center>
            <div class="col-md-12">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Section 1
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <p>A dedicated server is a single computer in a network reserved for serving the needs of the network.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Section 2
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <p>A dedicated server is a single computer in a network reserved for serving the needs of the network.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Section 3
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <p>A dedicated server is a single computer in a network reserved for serving the needs of the network.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </div>
</div>
</section>
<section class="home-newsletter" style="background-image: url(img/Home/dedicated_server1.jpg)">
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="single">
                <h2>Subscription</h2>
                <div class="input-group">
                    <input type="email" class="form-control" placeholder="Enter your email">
                    <span class="input-group-btn">
                        <button class="btn btn-theme" type="submit">Submit</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
