@extends('layouts.master12')
@section('title')
    DeckSys | Shared Linux and Windows Hosting, Dedicated Servers, VPS & Cloud Hosting, Coimbatore India
@endsection
@section('content')

<?php echo "<pre>";print_r($cartdata);exit;?>

@foreach($cartdata as $key=>$value)
<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
<input type="hidden" name="rowid" value="{{$value->rowId}}" />

@endforeach

<div class="container" style="max-width: 100% !important;">
    <div class="row pb-5" style="background-color: #fff;">
        <div class="offset-lg-2 col-lg-8 col-sm-12 p-5 border rounded main-section"style="margin-top:55px; background-color: ghostwhite;">
        <h1 class="text-center p-3" style="color:steelblue">Your Order</h1>
            <div class="flex">
                <div class="flex-1">
                    <label class="config-label">CORES:</label>
                    <br>
                    <p class="cpu order-des primary-sml-box">1</p>
                </div>
                <div class="flex-1">
                    <label class="config-label">RAM:</label>
                    <br>
                    <p class="ram order-des primary-sml-box">0.5 GB</p>
                </div>
                <div class="flex-1">
                    <label class="config-label">STORAGE:</label>
                    <br>
                    <p class="storage order-des primary-sml-box">20 GB</p>
                </div>
                <div class="flex-1">
                    <label class="config-label">BANDWIDTH:</label>
                    <br>
                    <p class="bandwidth order-des primary-sml-box">1 TB</p>
                </div>
                <div class="order-ind-sec order-month-price">
                    <div class="checkout-month-price white-box less-pad">
                    <p>Monthly Price</p>
                    <p class="checkout-price text-center"><i class="fa fa-inr" aria-hidden="true"></i>10000</p>
                    </div>
                </div>
            </div>
                <p class="offer-info"></p>
                <p class="vanilla-p powerpack-info">
                <em>
                You can change your server spec in our control panel after you have ordered
                </em>
                </p>
                <hr>
                <div class="order-sec-row flex space-between">
                    <div class="order-ind-sec flex-3" id="coupon-code">
                        <label class="config-label-cap d-block" for="user_coupon_code">Coupon Code</label>
                        <input class="order-input coupon-code text text" id="user_coupon_code" maxlength="50" name="user[coupon_code]" size="20" style="text-transform:uppercase;padding: 8px;" type="text">
                        <input id="festive50" name="festive50" type="hidden" value="true">
                        <input id="ssd_512_mb" name="ssd_512_mb" type="hidden" value="true">
                        <input id="ssd_1_OR_2_GB" name="ssd_1_OR_2_GB" type="hidden" value="false">
                        <input id="monthly_nodes" name="monthly_nodes" type="hidden">
                        <input id="least_used_cloud_id" name="least_used_cloud_id" type="hidden">
                        <button class="button-green btn-sm margin-left display-block" id="coupon_apply" type="button">Apply</button>
                    </div>
                    
                    <div class="order-ind-sec flex-1">
                        <h4 class="config-label-cap">Order Summary</h4>
                        <div class="order-summary">
                            <div class="flex space-between">
                                <div class="flex-2">Total</div>
                                <div class="flex-1 summary-price order_price"><i class="fa fa-inr" aria-hidden="true"></i>10000</div>
            
                            </div>
                            <div class="flex space-between to-pay-now">
                                <div class="flex-2">To pay now</div>
                                <div class="flex-1 summary-price order_net_pay"><i class="fa fa-inr" aria-hidden="true"></i>10000</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
@endsection