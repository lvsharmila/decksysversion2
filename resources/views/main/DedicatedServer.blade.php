@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')

<?php //echo "<pre>";print_r($products);exit;?>


<div class="container-fluid  text-center mb-0">
	<div class="row dedicatedServer">
	 <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<h2>BENEFITS WITH OUR DEDICATED SERVER :<strong style="color:#29539e;"> Affordable, Fast,
			<br> Secure, Complete </strong></h2>
	 	</div>
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 dedicatedServer"> 
	 		<div class="table-responsive">	 
	 			<form  role="form" onchange=sumValues()  method="post" action="{{route('cart.add')}}">
				 
				<table class="table">
		  	<thead>
				<tr>
					<th>PROCESSOR</th>
					<th>RAM</th>
					<th>BANDWIDTH/Mo</th>
					<th>STORAGE</th>
					<th>IP ADDRESS</th>
					<th>OPERATING SYSTEM</th>
					<th>DATABASE</th>
					<th>PRICE</th>
					<th>ORDER</th>
				</tr>
		  	</thead>
		  	<tbody>
					<tr>		
						<td>Xeon E3 1230v3 Quad Core Server</td>
						<td>8GB</td>
						<td>5TB</td>
						<td>1TB SATA/120 GB SSD</td>
						<td>1 IP</td>		
			  				@foreach($products['products']['product'] as $value)
									@if($value['gid'] == "6") 
										@if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
											@foreach($value['pricing'] as $key)                            
												@foreach($value['configoptions']['configoption'] as $configure)                                  
													@if($configure['name']=='Server Addons - OS')
														<td>
															<div class="form-group">
															<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
         											<input type="hidden" name="name" value="{{$value['name']}}" />
															 <input type="hidden" name="gid" value="{{$value['gid']}}" />				 
																<select class="form-control" name="os"id="box1">	
																	<option value="--select--">--select--</option>										
																		@foreach($configure['options']['option'] as $options)   
																			<option value='{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
																		@endforeach
																</select>
															</div>									
													</td>
												@elseif($configure['name']=='Server Addons - Database')
													<td>									
													<div class="form-group">
														<select class="form-control" name="db" id="box2">
															<option value="--select--">--select--</option>
																@foreach($configure['options']['option'] as $options)   
																	<option value='{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
																@endforeach
														</select>
													</div>
												</td>
											@endif
										@endforeach						
												<td>
													<input class="form-control" id='box3' type='hidden' value="{{$key['monthly']}}" />
													<input class="form-control" id='box4' type='text' name="total" value="{{$key['monthly']}}" readonly/>
												</td>
									@endforeach
								@endif
							</div>
						@endif
					@endforeach		
						    <td class="text-center"><button type="submit" class="btn btn-md btn-success">ORDER NOW</button></td>
				</tr>

			<!-- Xeon E3 1240v5 Quad Core Server -->
			<tr>
			  <td>Xeon E3 1240v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>
				@foreach($products['products']['product'] as $value)
					@if($value['gid'] == "6")  
						@if($value['name'] == "Xeon E3 1240v5 Quad Core Server")
							@foreach($value['pricing'] as $key)                            
								@foreach($value['configoptions']['configoption'] as $configure)                                  
									@if($configure['name']=='Server Addons - OS')
										<td>
											<div class="form-group">
												<select class="form-control" id="box5">	
												<option value="--select--">--select--</option>										
												@foreach($configure['options']['option'] as $options)   
													<option value='{{$options['pricing']['INR']['monthly']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
												@endforeach
												</select>
											</div>									
										</td>
									@elseif($configure['name']=='Server Addons - Database')
									<td>									
										<div class="form-group">
											<select class="form-control" id="box6">
													<option value="--select--">--select--</option>
													@foreach($configure['options']['option'] as $options)   
														<option value='{{$options['pricing']['INR']['monthly']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
													@endforeach
											</select>
										</div>
									</td>
									@endif
								@endforeach						
								<td>
									<input class="form-control" id='box7' type='hidden' value="{{$key['monthly']}}" />
									<input class="form-control" id='box8' type='text' value="{{$key['monthly']}}" readonly/>
								</td>
							@endforeach
						@endif
					</div>
				@endif
			@endforeach			
					<td class="text-center"><button type="submit" class="btn btn-md btn-success">ORDER NOW</button></td>
			</tr>

			<!-- Xeon E3 1270v5 Quad Core Server -->

			<tr>
			  <td>Xeon E3 1270v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>	
				@foreach($products['products']['product'] as $value)
			 		 @if($value['gid'] == "6")   
							@if($value['name'] == "Xeon E3 1270v5 Quad Core Server")
								@foreach($value['pricing'] as $key)                            
									@foreach($value['configoptions']['configoption'] as $configure)                                  
										@if($configure['name']=='Server Addons - OS')
										<td>
											<div class="form-group">
												<select class="form-control" id="box9">	
													<option value="--select--">--select--</option>										
													@foreach($configure['options']['option'] as $options)   
														<option value='{{$options['pricing']['INR']['monthly']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
													@endforeach
												</select>
											</div>									
										</td>
										@elseif($configure['name']=='Server Addons - Database')
									<td>									
										<div class="form-group">
											<select class="form-control" id="box10">
												<option value="--select--">--select--</option>
												@foreach($configure['options']['option'] as $options)   
													<option value='{{$options['pricing']['INR']['monthly']}}'>{{$options['pricing']['INR']['monthly']}} - {{$options['name']}}</option>
												@endforeach
											</select>
										</div>
								</td>
									@endif
							@endforeach						
						<td>
							<input class="form-control" id='box11' type='hidden' value="{{$key['monthly']}}" />
							<input class="form-control" id='box12' type='text' value="{{$key['monthly']}}" readonly/>
						</td>
					@endforeach
				@endif
			</div>
		@endif
	@endforeach	
			  <td class="text-center"><button type="button" class="btn btn-md btn-success">ORDER NOW</button></td>
			</tr>
		</tbody>
	</table>
</form>
</div>
</div>
  </div>
</div><!-- Dedicated Server  : Table End-->




<!-- Dedicated Server Features Section-->	
<section class="dedicated_features">
<div class="container text-center">
  <h2><b>FEATURES</b></h2>         
	  <table class="table table-bordered " style="box-shadow:20px 20px 40px grey;">
		<thead>
		  <tr>
			<th style="font-size:19px;background-color: #07539e;color:white;"><b>Features</b></th>
			<th class="bg-dark text-white" style="font-size:19px;">Dedicated Server</th>
		  </tr>
		</thead>
		<tbody>
		  <tr>
			<td class="features">Web site hosting</td>
			<td><strong>$10</strong></td>
		  </tr>
		  <tr>
			<td class="features">Root Access</td>
			<td><img  src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features1"></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">24x7 Support</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Optional Support</td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}} " height="30px" alt="dedicated_features2" ></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">Free SSL</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Dedicated hosting</td>
			<td><strong>$100</strong></td>
		  </tr>			  </tr>
		  <tr>
			<td class="features">Free Website Migration
				<a href="#" data-toggle="tooltip" title="Free website migration is available on Dedicated Server purchased for a minimum 3 months tenure." style="color: #337ab7;
				 text-decoration: none;cursor:point;background-color: transparent;"><span class="glyphicon glyphicon-info-sign"></span>
				 <img src="{{asset('img/ico/Information.png')}}" alt="Information.png" >
				 </a></td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features2" ></td>
		  </tr>
		</tbody>
	  </table>
</div>
</section>


<script type='text/javascript'>
   function sumValues() {
      var v1 = parseInt(document.getElementById('box1').value);
      var v2 = parseInt(document.getElementById('box2').value);
	  var v3 = parseInt(document.getElementById('box3').value);
		var v4=v1+v2+v3;
		if (isNaN(v2)) {
     return 0;
   }
//	console.log(v2,v1);
	 
      document.getElementById('box4').value = v4;

	 	
			var v5 = parseInt(document.getElementById('box5').value);
      var v6 = parseInt(document.getElementById('box6').value);
	  var v7= parseInt(document.getElementById('box7').value);
		var v8=v5+v6+v7;
		if (isNaN(v6)) {
     return 0;
   }
//	console.log(v2,v1);
	 
      document.getElementById('box8').value =  v8;

			var v9 = parseInt(document.getElementById('box9').value);
      var v10 = parseInt(document.getElementById('box10').value);
	  var v11= parseInt(document.getElementById('box11').value);
		var v12=v9+v10+v11;
		if (isNaN(v10)) {
     return 0;
   }
//	console.log(v2,v1);
	 
      document.getElementById('box12').value =  v12;

		 
		

   }
</script>

@endsection
