@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')


<section class="locations">
<div class="container-fluid  text-center mb-0">
   <div class="row ">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
           <h2> <b> CHOOSE FROM OUR <strong style="color:#29539e;"> DATA CENTERS</strong> <br> ACROSS 25 CONTRIES </b></h2>
        </div>
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 locations"> 
           <p>Select the best <strong class="location-heading">DeckSys </strong>server locations for fast response times, keen pricing, data regulation compliance and to create a local presence.</p>
       </div>
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "> 
           <img src="https://cdn.zeemaps.com/images/base/map.png" alt="Map" width="auto" height="500px" class="text-center img-fluid">
       </div>
   </div>
</div><!-- Locations  : Map row 1-->
<div class="container">
   <div class="row">
       <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
           <div  class="row">
               <div class="offset-xl-2 col-xl-4 col-lg-6 col-md-6 col-sm-6 col-6">
                   <h4 class="location-heading">Asian Cloud Datacenters</h4><br>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp;New Delhi, India</p>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp; Singapore, Singapore</p>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp; Tokyo, Japan</p><br>
               </div>
               <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                   <h4 class="location-heading">Canadian Cloud Datacenters</h4><br>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp;Los Angeles, California</p>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp;Atlanta, Georgia</p>
                       <p class="location-headingContent"><img  src="/img/ico/Location-india.png">&nbsp;Atlanta, Georgia</p><br>
               </div>
           </div>
       </div>
       </div>
   <div class="container "> 
       <div class="row py-4 text-center">
           <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="card text-white bg-dark mb-3" >
                 <div class="card-body ">
                   <h5 class="card-header">CLOUDS LOCATED ACROSS THE GLOBE</h5>
                   <p class="card-text"><strong class="location-heading">DECKSYS.COM </strong> maintains 24 cloud data center locations worldwide. 24 locations allows you to strategically place your website in the cloud that best fits you and your website's needs. Choosing the right data center is important for the success of your website. By choosing a location close to you, you're able to manage your site with an extremely fast connection speed. </p>
                   <a href="#" class="btn btn-primary">Go somewhere</a>
                 </div>
               </div>
           </div>
           <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="card text-white bg-dark mb-3" >
                 <div class="card-body">
                   <h5 class="card-header">DATA CENTER INFASTRUCTURE</h5>
                   <p class="card-text">
                       We are very careful when we choosing a new data center to build our cloud in. Our data centers undergo series of security, performance and procedural tests before a cloud is ever placed in the location. <strong class="location-heading">DECKSYS.COM </strong> routinely performs follow up tests to confirm continued compliance of our established requirements.
                   </p><br>
                   <a href="#" class="btn btn-primary">Go somewhere</a>
                 </div>
               </div>
           </div>
       </div><!-- Locations Card section row 1 End -->
       <div class="row">
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-power.png"  alt="Location-power">
                     </div>
                     <div class="media-body pt-3 pl-2">Tier 3 classification </div>
                   </div>
             </div>
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-button-check_blue.png"  alt="Location-button-check_blue">
                     </div>
                     <div class="media-body pt-3 pl-2">Redundant power sources </div>
                   </div>
             </div>
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-setting.png"  alt="Location-setting.png">
                     </div>
                     <div class="media-body pt-3 pl-2">On-site professional staff to monitor and provide assistance </div>
                   </div>
             </div>
          </div><!--Container Col1 End -->
          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
               <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-power.png"  alt="Location-power1">
                     </div>
                     <div class="media-body pt-3 pl-2">Smoke detection system </div>
                   </div>
             </div>
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-button-check_blue.png"  alt="Location-button-check_blue1">
                     </div>
                     <div class="media-body pt-3 pl-2">24x7 security and digital video surveillance </div>
                   </div>
             </div>
             <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 py-1">
                   <div class="media">
                     <div class="media-left">
                         <img class="media-object img-fluid" src="/img/ico/Location-setting.png"  alt="Location-setting.png1">
                     </div>
                     <div class="media-body pt-3 pl-2">Multiple network carriers providing unsurpassed reliability </div>
                   </div>
             </div>
          </div><!--Container Col2 End -->

       </div><!--Container Row End -->

       
       <div class="row py-3 text-center" style="box-shadow:20px 20px 40px grey;">
           <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
               <h5 style="font-size:2vw;padding-bottom:10px;">  SERVICES OFFERED IN OUR <strong style="color:#29539e"> DATA CENTERS</strong>  </h5><hr>
           </div>
       <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="row">
               <div class="col-12">
                   <img src="/img/ico/Location-cloud.png" width="150px" height="150px">
               </div>
               <div class="col-12">
                   <h4><strong class="location-heading">CLOUD VPS </strong></h4> 
                </div>
               <div class="col-12 p-4">
                   <p>High availability Virtual Private Server, built on our self healing Cloud. Best for reliability.</p>
               </div>
               <div class="col-12 pb-2" >
                   <button class="button btn btn-info btn-lg buyNow">BUY NOW</button>
               </div>
            </div>
       </div>
       <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="row">
               <div class="col-12 pb-4">
                   <img src="/img/ico/location-ssd.png" width="150px" height="130px">
               </div>
               <div class="col-12">
                   <h4><strong class="location-heading">SSD VPS </strong></h4> 
                </div>
               <div class="col-12 p-4">
                   <p>High performance, low price Virtual Private Servers, with SSD Disks. Best for price & performance</p>
               </div>
               <div class="col-12 pb-2" >
                   <button class="button btn btn-info btn-lg buyNow">BUY NOW</button>
               </div>
            </div>
       </div>
       </div><br>
    </div>
   </div><!-- Locations  : Map row -->	
</section>
    
<!-- Data Center Locations Section-->	
@endsection
