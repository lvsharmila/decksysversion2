@extends('layouts.master')

@section('title')
    DashBoard
@endsection
@section('content')

<section class="vpsServer">
<div class="container-fluid">
    <div class="row py-5">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                 <h2 class="text-center pb-4 "><b>
                    VPS SERVER MONITORING SERVICES WITH <strong style="color:#29539e;">PROACTIVE SUPPORT</strong>
                </b>
                </h2>
                <p class="card text-center p-4">
                    At decksys.com we offer two types of managed services solutions:
                    Server Monitoring and Monitoring + Configuration. These services are ideal for customers who wish to maintain a hands-off approach for their web hosting services.
                    For example, if you would you like us to help you configure your server, simply let us know the requirements and we’ll take over for you. Our friendly and helpful in-house expertise is available 24/7/365.
                </p>
            </div>
            <div class="container">
            
            
                <table class="table-bordered" width="100%" >
              <thead>
                <tr>
                  <th class="pro"></th>
                  <th class="prod">Server Monitoring</th>
                  <th class="prod">Monitoring & Configuration</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="features">Monitoring <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle"><img   src="/img/ico/competitors-yes.png"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                </tr>
                <tr>
                  <td class="features">Proactive Support <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                </tr>
                <tr>
                  <td class="features">Response SLA <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle">30 Minute</td>
                  <td align="center" valign="middle">15 Minute</td>
                </tr>
                  <tr>
                    <td class="features">Backup Setup <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Assisted Start-up <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Server Configuration <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Site Migration <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Server Security <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Price</td>
                    <td align="center" valign="middle">$15.00/mo</td>
                    <td align="center" valign="middle">$30.00/mo</td>
                  </tr>
              </tbody>
            </table>
    </div>
</div>
</section>

<section class="supportFeatures">
<div class="container-fluid">
    <h2 class="text-center py-4">SUPPORTED BY <strong style="color:#29539e;">VPS MANAGED SERVICES</strong> </h2><hr>
    <p class="text-center px-5 py-1">Our support team has the expertise to help with any of the areas listed below. Can’t find your issue or item on the list? <a class="text" href="">Contact us here</a> and we will assist you with all of your questions.</p>
</div>
<div class="container">
    <div class="row ml-4 ">
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>Protocols and Ports Covered By Monitoring</strong></h5>
            <ul >
                <li>FTP (21)</li>
                <li>SFTP/SSH (22)</li>
                <li>HTTP (80)</li>
                <li>HTTPS (443)</li>
                <li>IMAP (143, 993)</li>
                <li>POP (110, 995)</li>
                <li>SMTP (25, 587)</li>

            </ul>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>Security Configuration</strong></h5>
            <ul >
                <li>ModSecurity</li>
                <li>Rkhunter (Scan)</li>
                <li>SSH Lockdown</li>
                <li>SSL Installation and Troubleshooting</li>
                <li>Firewall (CSF+LFD, APF, PFsense)</li>
                <li>ClamAV (Linux)(Scan)</li>
                <li>Maldet (Scan)</li>

            </ul>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>Networking & DNS</strong></h5>
            <ul >
                <li>Custom Nameservers</li>
                <li>DNS Zone File and Record Management</li>
                <li>Basic Networking Troubleshooting</li>
                <li>Basic Network Configuration</li>
                <li>IP Configuration</li>
                <li>Spam Filtering (SpamAssassin)</li>
            </ul>
        </div>
    </div>
    <div class="row ml-4">    
        <div class="col-6 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>Linux</strong></h5>
            <ul >
                <li>Cron/Anacron Configuration</li>
                <li>Data Mirroring: scp, rsync, lsftp, dd</li>
                <li>CentOS</li>
                <li>Debian</li>
                <li>Fedora</li>
                <li>Ubuntu</li>
                <li>RedHat Enterprise Linux</li>
                <li>File permissions Troubleshooting</li>
                <li>File System Management</li>
                <li>High Load Management</li>
                <li>Kernel Upgrades</li>
                <li>LAMP Install & Configuration</li>
                <li>Linux Kernel Modules</li>
            </ul>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>cPanel</strong></h5>
            <ul >
                <li>cPanel Configuration</li>
                <li>cPanel Installation</li>
                <li>cPanel to cPanel Migration</li>
                <li>DNS Clustering Configuration</li>
                <li>PHP Modules in EasyApache </li>
                <li>cPanel Backup Configuration</li>
                <li>Database Restoration (from local DB or with .sql file)</li>
            </ul>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-4 ">
            <h5 class="managedServices-heading"><strong>Software / Applications Assistance</strong></h5>
            <ul >
                <li>Apache Configuration</li>
                <li>Dovecot</li>
                <li>EasyApache</li>
                <li>Exim</li>
                <li>Mail Server Troubleshooting (Exim, Postfix, Sendmail, Smartermail)</li>
                <li>MySQL Setup, Configuration and Troubleshooting</li>
                <li>Nginx Configuration</li>
                <li>PHP 5.x-7 Management and Configuration</li>
                <li>R1soft</li>
                <li>Server Density Installation and Troubleshooting</li>
                <li>Softaculous Installation</li>
                <li>Webmail (Horde/Squirrelmail/Roundcube)</li>
                <li>WordPress</li>
                <li>Best effort support for 3rd Party Applications not listed</li>
            </ul>

        </div>
    </div>
</div>
</section>

<section class="call-now" >
    <div class="container-fluid">
         <h2 class="text-center h2-mt py-4">NEXT STEPS</h2>
         <p class="text-center lines pb-4">
        New customers can order their preferred Managed Services & Support package during the ordering process on our
         <a class="text" href="/VPS">SSD VPS</a> or
         <a class="text" href="/DedicatedServer">Dedicated Srever</a> product pages. Existing customers simply need to
         <a class="text" href="/img/ico/login.png">log into their account</a> to order new Managed Services & Support. For further information, call
         +91 84484 44086
          or <a class="text" href="#Comm100API.open_chat_window(event, 1529);">Chat Live</a> with our sales team.
      </p>
      

      <div class="container text-center">
      <div class="row py-4">
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">LOG IN TO ORDER</h5>
          <a href="#"class="btn btn-primary btn-capsul box px-4 py-2"><img src="/img/ico/login-icon.png" class="call-now-icon-helper "> LOG IN TO ACCOUNT</a>
        </div>
  
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">FOR FURTHER INFORMATION</h5>
                            <a class="text" href="+91 84484 44086" ><img src="/img/ico/telephone-icon.png" > CALL <strong>NOW ON</strong> +91 84484 44086</a>
                    </div>
  
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">CHAT LIVE TO SALES TEAM</h5>
          <a href="#" class="btn btn-primary btn-capsul box px-4 py-2"><img src="/img/ico/chat-icon.png" > LIVE CHAT</a>
        </div>
      </div>
    </div>
    </div>
  </section>	
@endsection
