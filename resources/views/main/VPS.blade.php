@extends('layouts.master')

@section('title')
    Decksys | VPS
@endsection
@section('content')

<?php //echo "<pre>";print_r($products);exit;?>

<section class="specificationSection pt-4 pb-4" >
<div class="text-center p-3">
    <h2><b>SELECT YOUR SSD SIZE FOR SERVER SPECIFICATION</b></h2>
</div>
<div class="container-fluid">
    <div class="row no-gutters ">
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
            <div class="vpsHolder">
                <div class="vpsContent">
                    <div class="textHeader text-center text-white  p-2">
                        <h4>VPS 0.5GB</h4>
                    </div>
                    <div class="textContent text-center bg-white p-2">
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 0.5GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach                                       
                       <p><strong>0.5GB</strong> RAM</p>
                        <p><strong>16GB</strong> SSD Disk</p>
                        <p><strong>2TB</strong> Bandwidth</p>
                        <p><strong>1</strong> Core</p>
                        <label><input class="checkbox" id="myCheck"  type="radio" name="ssdProduct" value="" onclick="myFunction()">
                        <img/></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
            <div class="vpsHolder">
                <div class="vpsContent">
                    
                    <div class="textHeader text-center text-white  p-2">
                        <h4>VPS 1GB</h4>
                    </div>
                    <div class="textContent text-center bg-white  p-2">
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 1GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach
                        <p><strong>1GB </strong>RAM</p>
                        <p><strong>20GB </strong>SSD Disk</p>
                        <p><strong>3TB </strong>Bandwidth</p>
                        <p><strong>4 </strong>Core</p>
                        <label><input class="checkbox" id="myCheck1" type="radio"  name="ssdProduct" value=""  onclick="myFunction()">
                        <img/></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3"> 
            <div class="vpsHolder">
                <div class="vpsContent">
                    
                    <div class="textHeader text-center text-white p-2">
                        <h4>VPS 2GB</h4>
                    </div>
                    <div class="textContent text-center bg-white p-2 ">
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 2GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach
                        <p><strong>3GB </strong>RAM</p>
                        <p><strong>22GB </strong>SSD Disk</p>
                        <p><strong>1TB </strong>Bandwidth</p>
                        <p><strong>4 </strong>Core</p>
                        <label><input class="checkbox" id="myCheck2" type="radio"  name="ssdProduct" value=""  onclick="myFunction()" >
                        <img/></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
                <div class="vpsHolder">
                    <div class="vpsContent">
                        <div class="textHeader text-center text-white p-2">
                            <h4>VPS 4GB</h4>
                        </div>
                        <div class="textContent text-center bg-white   p-2">
                        @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 4GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach
                            <p><strong>3GB </strong>RAM</p>
                            <p><strong>20GB </strong>SSD Disk</p>
                            <p><strong>1TB </strong>Bandwidth</p>
                            <p><strong>4 </strong>Core</p>
                            <label><input class="checkbox" id="myCheck3" type="radio"  name="ssdProduct" value=""  onclick="myFunction()" >
                            <img/></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
                    <div class="vpsHolder">
                        <div class="vpsContent">
                            <div class="textHeader text-center text-white p-2">
                                <h4>VPS 8GB</h4>
                            </div>
                            <div class="textContent text-center bg-white p-2 ">
                            @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 8GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach
                                <p><strong>2GB </strong>RAM</p>
                                <p><strong>24GB </strong>SSD Disk</p>
                                <p><strong>1TB </strong>Bandwidth	</p>
                                <p><strong>4 </strong>Core</p>
                                <label><input class="checkbox" id="myCheck4" type="radio"  name="ssdProduct" value=""  onclick="myFunction()" >
                                <img/></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
                    <div class="vpsHolder">
                        <div class="vpsContent">
                            
                            <div class="textHeader text-center text-white p-2">
                                <h4>VPS 16GB</h4>
                            </div>
                            <div class="textContent text-center bg-white p-2">
                            @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 16GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <h3 class="subHeader p-3"><strong><i class="fa fa-inr"></i>{{$total['monthly']}}<sub>/mo</sub></strong></h3>   
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach
                                <p><strong>1GB </strong>RAM</p>
                                <p><strong>25GB </strong>SSD Disk</p>
                                <p><strong>3TB </strong>Bandwidth</p>
                                <p><strong>4 </strong>Core</p>
                                <label><input class="checkbox" id="myCheck5" type="radio"  name="ssdProduct" value=""  onclick="myFunction()">
                                <img/>
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
</section>

<section class="container-fluid selectOS p-3">
<div class="selectOSHeader text-center">
    <h4 class="headerOne" >SELECT YOUR OS:</h4>
</div>
<div class="row no-gutters selectOScontent p-4">
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
 <label class="radio-inline">
            <input type="radio" class="checkbox" name="opt" id="myCheck6" value=""  onclick="myFunction()" > CentOS 6
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
            <input type="radio"  class="checkbox" name="opt" id="myCheck7" value=""  onclick="myFunction()"> Ubuntu
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
        <input type="radio"  class="checkbox" name="opt" id="myCheck8" value=""  onclick="myFunction()"> Plesk
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
        <input type="radio"  class="checkbox" name="opt" id="myCheck9" value=""  onclick="myFunction()"> CPanel
        </label>
    </div>
  
    <!-- <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 " >
        <label class="radio-inline">
            <input type="radio" name="opt" id="opt5"> Ubundu 18.04
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
            <input type="radio" name="opt" id="opt6"> windows 2012
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
            <input type="radio" name="opt" id="opt7"> CentOs with CPanel
        </label>
    </div>
    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3" >
        <label class="radio-inline">
            <input type="radio" name="opt" id="opt8"> Option 8
        </label>
    </div> -->
</div>		  
</section>
<!-- 
<section class="configurationOS p-3">
<div class="configurationOSHeader text-center" >
<h4 class="headerTwo">MANAGED SERVICE:</h4>
</div>

<div class="row no-gutters p-4">
    <div class="col-sm-12 col-md-6 col-lg-6">
        <label class="radio-inline">
            <input type="radio" name="opt1" checked> Option1
        </label>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-6" >
        <label class="radio-inline">
            <input type="radio" name="opt1"> Option2
        </label>
    </div>
</div>
</section> -->
<section class="pricingSection p-1">
<div class="container-fluid">
<hr>
<div class="row no-gutters pricingcontent pl-4">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pl-5 priceBorder" >
            <h3 class="country-drop text-center">PRICE</h3>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pl-2 priceBorder text-center" style="">
            <h2 class="priceContent"><strong>
            @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 0.5GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="currency"></span>  
                                            <span class="price-val" id="text" style="display:none"><i class="fa fa-inr"></i>package : {{$var=$total['monthly']}}/mo</span> 
                                            @endforeach
                                            @elseif($key['name'] == "VPS 1GB")
                                            @foreach($key['pricing'] as $total1)
                                            <span class="price-val" id="text1" style="display:none"><i class="fa fa-inr"></i>package : {{$var1=$total1['monthly']}}/mo</span>   
                                            @endforeach
                                            @elseif($key['name'] == "VPS 2GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="price-val" id="text2" style="display:none"><i class="fa fa-inr"></i>package : {{$var2=$total['monthly']}}/mo</span>   
                                            @endforeach
                                            @elseif($key['name'] == "VPS 4GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="price-val" id="text3" style="display:none"><i class="fa fa-inr"></i>package : {{$var3=$total['monthly']}}/mo</span>   
                                            @endforeach
                                            @elseif($key['name'] == "VPS 8GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="price-val" id="text4" style="display:none"><i class="fa fa-inr"></i>package : {{$var4=$total['monthly']}}/mo</span>   
                                            @endforeach
                                            @elseif($key['name'] == "VPS 16GB")
                                            @foreach($key['pricing'] as $total)
                                            <span class="price-val" id="text5" style="display:none"><i class="fa fa-inr"></i>package : {{$var5=$total['monthly']}}/mo</span>   
                                            @endforeach


                                       @endif
                                    @endif
                                @endforeach


                                @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "16")                               
                                @if($value['name'] == "VPS 0.5GB")
                                    @foreach($value['pricing'] as $key)                                       
                                                @foreach($value['configoptions']['configoption'] as $configure)                                  
                                                    @if($configure['name']=='VPS 0.5GB')
                                                        @foreach($configure['options']['option'] as $options)                                           
                                                            @if($options['name'] == 'CentOS 6')
                                                            <span class="price-val" id="text6" style="display:none">os :{{$p1=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var+$p1}}/mo</span>   
                                                            @elseif($options['name'] == 'Ubuntu')
                                                            <span class="price-val" id="text7" style="display:none">os :{{$p2=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var+$p2}}/mo</span>
                                                            @elseif($options['name'] == 'Plesk')
                                                            <span class="price-val" id="text8" style="display:none">os :{{$p3=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var+$p3}}/mo</span>
                                                            @elseif($options['name'] == 'CPanel')
                                                            <span class="price-val" id="text9" style="display:none">os :{{$p4=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var+$p4}}/mo</span>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach           
                                    @endforeach    
                                
                                @elseif($value['name'] == "VPS 1GB")
                                    @foreach($value['pricing'] as $key)                                       
                                                @foreach($value['configoptions']['configoption'] as $configure)                                  
                                                    @if($configure['name']=='VPS 1GB')
                                                        @foreach($configure['options']['option'] as $options)                                           
                                                            @if($options['name'] == 'CentOS 6')
                                                            <span class="price-val" id="text6" style="display:none">os :{{$p1=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var1+$p1}}/mo</span>   
                                                            @elseif($options['name'] == 'Ubuntu')
                                                            <span class="price-val" id="text7" style="display:none">os :{{$p2=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var1+$p2}}/mo</span>
                                                            @elseif($options['name'] == 'Plesk')
                                                            <span class="price-val" id="text8" style="display:none">os :{{$p3=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var1+$p3}}/mo</span>
                                                            @elseif($options['name'] == 'CPanel')
                                                            <span class="price-val" id="text9" style="display:none">os :{{$p4=$options['pricing']['INR']['monthly']}}/mo <br> Total :{{$var1+$p4}}/mo</span>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endforeach           
                                    @endforeach    
                                @endif

                                
                            @endif
                        @endforeach
   



  

                      </strong>
            <!-- <span class="ssd-price-term">/mo</span> <h2> -->
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pl-2 text-center" >
                <button class="button btn btn-info btn-lg">BUY NOW</button>
        </div>
</div>
</div>
<hr>
</section>
<section class="featuresSection pt-4 pb-4" >
<div class="container Vps">

<div class="text-center ">
    <h2><strong>FEATURES</strong></h2>
</div>
<div class="container-fluid pt-3">
    <table class="table " style="border-color:1px solid red">
        <thead>
            <tr>
                <th class="bg-info pl-5" style="width:450px;">Features</th>
                <th class="pl-5" style="width:450px;">SSD-VPS</th>
            </tr>
        </thead>
        <tbody>
            <tr class="bg-active">
                <td class="pl-5">Price (from)</td>
                <td class="pl-5">$5</td>
            </tr>      
            <tr>
                <td class="pl-5">Web site hosting</td>
                <td class="pl-5"><img src="{{asset('img/ico/dedicated_features2.png ')}}" height="30px" alt="dedicated_features2" >
                </td>
            </tr>
            <tr>
                <td class="pl-5">Application hosting</td>
                <td class="pl-5"><img src="{{asset('img/ico/dedicated_features2.png ')}}" height="30px" alt="dedicated_features2" >
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div>
</section>
@endsection
