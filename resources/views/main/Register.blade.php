@extends('layouts.master12')
@section('title')
    Register
@endsection
@section('content')
   
<div class="container"style="max-width: 100% !important;margin-top:25px;background-color: #fff;">
<div class="row p-5">
        <div class="offset-lg-2 col-lg-8 col-sm-12  border rounded main-section" style="background-color: ghostwhite;">
              
                <?php if(isset($_GET['error'])){ ?>
       
<p style="text-align: center;color: red;font-size: 18px;">A user already exists with that email address</p>  
      <?php } ?>
      <h3 class="text-center p-3" style="color:steelblue">Register</h3>
                <form class="container"  id="needs-validation" action="{{route('user.insert')}}" method="post" novalidate  autocomplete="false" style="margin-top: 30px;">
                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="validationCustom01">First Name<span class="req">*</span></label>
                                <input type="text" name="firstname" class="form-control" id="validationCustom01"  placeholder="First name"
                                       value="" required maxlength="25" onKeyPress="return ValidateAlpha(event);" >
                                <div class="invalid-feedback">
                                    Enter Your Firstname
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="validationCustom02">Last Name<span class="req">*</span></label>
                                <input type="text" name="lastname" class="form-control" id="validationCustom02" placeholder="Last name" value="" required maxlength="25" onKeyPress="return ValidateAlpha(event);" >
                                <div class="invalid-feedback">
                                    Enter Your Lastname
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputEmail4">Email<span class="req">*</span></label>
                                <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email" required maxlength="250">
                                <div class="invalid-feedback">
                                    Verify Your Email Id
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">

                                <label class="text-inverse" for="inputphone">Phone Number<span class="req">*</span>
                                </label>
                                <input  type="tel" name="phonenumber" class="form-control"  placeholder="Phone Number" required onkeypress="return isNumberKey(event)" maxlength="10" >



                                <div class="invalid-feedback">
                                    Enter Correct Mobile Number
                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputEmail4">Address 1<span class="req">*</span></label>
                                <input type="text" name="address1" class="form-control" id="inputEmail4" placeholder="Address1" maxlength="75">
                                <div class="invalid-feedback">
                                    Enter Your Address
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6 col-sm-6 col-12">
                          <div class="form-group">
                            <label class="text-inverse custom" for="inputpassword">Address 2</label>
                            <input type="text" name="address2" class="form-control" id="inputpassword" placeholder="Address2" style="background-color: rgba(19, 35, 47, 0.4); color:#fff">

                          </div>
                        </div> -->

                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputEmail4">Address 2</label>
                                <input type="text" name="address2" class="form-control" id="inputEmail4" maxlength="25" placeholder="Address2">
                                <div class="invalid-feedback">
                                    Enter Your Address
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="validationCustom03">Company Name</label>
                                <input type="text" name="companyname" class="form-control" id="companyname" placeholder="Company Name" >

                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">

                            <div class="form-group">
                                <label class="text-inverse" for="inputEmail4">Country<span class="req">*</span></label>
                                <select name="country" class="custom-select d-block form-control countries">
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Aland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua And Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia And Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, Democratic Republic</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">Cote D'Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">Curacao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard Island & Mcdonald Islands</option>
                                    <option value="VA">Holy See (Vatican City State)</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN" selected="selected">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran, Islamic Republic Of</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle Of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KR">Korea</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libyan Arab Jamahiriya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macao</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States Of</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
									<option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="AN">Netherlands Antilles</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestine, State of</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Reunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="BL">Saint Barthelemy</option>
                                    <option value="SH">Saint Helena</option>
                                    <option value="KN">Saint Kitts And Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="MF">Saint Martin</option>
                                    <option value="PM">Saint Pierre And Miquelon</option>
                                    <option value="VC">Saint Vincent And Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome And Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia And Sandwich Isl.</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard And Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad And Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks And Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands, British</option>
                                    <option value="VI">Virgin Islands, U.S.</option>
                                    <option value="WF">Wallis And Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                                <div class="invalid-feedback">
                                    Select Your Country
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="select-menu">State<span class="req">*</span></label>

                                <label for="stateinput" class="field-icon" id="inputStateIcon">
                                    <i class="fa fa-map-signs"></i>
                                </label>
                                <input type="text" name="state" id="state" class="field form-control" placeholder="State" value="" >

                            </div>
                            {{--<div class="invalid-feedback1">--}}
                            {{--Select Your State--}}
                            {{--</div>--}}
                        </div>

                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="select-menu">City<span class="req">*</span></label>

                                <input type="text" name="city" class="form-control" id="inputEmail4" placeholder="City" required maxlength="10" onKeyPress="return ValidateAlpha(event);">
                                <div class="invalid-feedback">
                                    Enter Your city
                                </div>
                            </div>

                        </div>

                        
                    </div>

<div class="row">
<div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputEmail4">Zip Code<span class="req">*</span></label>
                                <input type="text" name="postcode" class="form-control" id="inputEmail4" placeholder="Zip Code" required onkeypress="return isNumberKey(event)" maxlength="6">
                                <div class="invalid-feedback">
                                    Enter Your Zip Code
                                </div>
                            </div>
                        </div>

                    <div class="col-sm-6">
<div class="form-group">
<label for="customfield1" class="text-inverse">GSTNumber</label>
<div class="control">
<input type="text" name="customfield[1]" id="customfield1" placeholder="GST Number" value="" size="30" class="form-control">
                        </div>
</div>
</div>
                                    <div class="clearfix"></div>
            </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputpassword">Password<span class="req">*</span></label>
                                <input type="password" name="password" class="form-control" name="password" id="password" placeholder="Password" required="required">
                                <div class="invalid-feedback">
                                    Enter Your Password
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-12">
                            <div class="form-group">
                                <label class="text-inverse" for="inputpassword">Confirm Password<span class="req">*</span></label>
                                <input type="password" name="password2" class="form-control" id="confirmpassword" placeholder="Confirm Password" required="required">
                                <span id='message'></span>
                                <div class="invalid-feedback">
                                    Enter Your Confirm Password
                                </div>
                            </div>
                        </div>
                        <div id="pswd_info">
                        <h4>Password must meet the following requirements:</h4>
                        <ul style="list-style-type:none;">
                            <li id="letter" class="invalid">At least <strong>one letter</strong></li>
                            <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
                            <li id="number" class="invalid">At least <strong>one number</strong></li>
                            <li id="special" class="invalid">At least <strong>one special character</strong></li>
                            <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
                        </ul>
                    </div>
                    </div>

                    <div class="row">
                    
                                            <div class="col-lg-12 col-sm-6 col-12">
                                                <div class="form-group">
                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox"   id="inputpassword" required>
                    
                                                        <!-- <span class="custom-control-indicator"></span> -->
                    
                                                        <span class="custom-control-description"><a href="Terms_Service">I have read and agree to Terms and Conditions</a></span>
                                                    </label>
                    
                                                </div>
                                            </div>
                                        </div>

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center" style="padding-bottom: 20px;">
                            <button class="btn btn-info" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection