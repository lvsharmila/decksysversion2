<footer class="appFooter">
        <section class="footerSec1 p-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <h4> SSD VPS</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">Cloud VPS</a></li>
                            <li><a href="#">Cloud Hosting -SME</a></li>
                            <li><a href="#">Migration</a></li>
                            <li><a href="#">Maintenance</a></li>
                            <li><a href="#">Private Cloud</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <h4> Managed Services</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">eCommerce Hosting</a></li>
                            <li><a href="#">Business Hosting</a></li>
                            <li><a href="#">eMail Hosting</a></li>
                            <li><a href="#">Database Management</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <h4>Dedicated Server</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">Solution Partners</a></li>
                            <li><a href="#">Reseller Program</a></li>
                            <li><a href="#">Affiliate Program</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <h4>Support</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">Shared Hosting</a></li>
                            <li><a href="#">Managed Wordpress</a></li>
                            <li><a href="#">Managed Hosting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <h4>Blog</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">Bare Metal Servers</a></li>
                            <li><a href="#">Linux KVM</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                        <h4>Datacenter Location</h4>
                        <hr>
                        <ul class="footerLinks">
                            <li><a href="#">Brand</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <div class="socialConnect">
                            <h4>Find us on Social</h4>
                            <hr>
                            <ul class="socialItems">
                                <li><a href="#"><img class="media-object" src="{{asset('img/ico/fb-icon.png')}}"   alt="fb"></a></li>
                                <li><a href=""><img class="media-object" src="{{asset('img/ico/twitter-icon.png')}}"  alt="tw"></a></li>
                                <li><a href=""><img class="media-object" src="{{asset('img/ico/linkedin-icon.png')}}"  alt="In"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                        <h4>Contact Address</h4>
                        <hr>
                        <div class="footerContact">
                            <div class="media">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="media-object" src="{{asset('img/logo/logo-makto.png')}}"  alt="...">
                                    </a>
                                </div>
                                <div class="media-body pl-3">
                                    <h5 class="media-heading">Makto Technology Pvt Ltd</h5>
                                        <p >B2, First Floor, Rathinam Technical Campus,
                                            Pollachi Main Road, Eachanari,
                                            Coimbatore 641 021.
                                        </p>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
            </section>
            
            <section class="footerSec2 pt-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-12">
                            <div class="copyrights">
                                <ul class="policy">
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Terms and Conditions</a></li>
                                    <li><a href="#">Refund Policy</a></li>
                                </ul>
                                <div>
                                    <ul class="copyright">
                                        <li>
                                            <p>Copyrights 2018. Makto Technology Pvt Ltd.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </footer>