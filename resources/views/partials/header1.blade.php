<header class="appHeader">
        <div class="navOne">
            <div class="container-fluid"> 
                <div class="row no-gutters">
                    
                    <div class="col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                        <div class="userServices" data-toggle="modal" data-target="#requestQuoteModal">
                        <img src="{{asset('img/ico/phone.png')}}" alt="Get_Quote">
                           
                           
                            <p>+91 84484 44086</p>
                        </div>
                       
                    </div>
                    
                    
                    <div class="col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                        <div class="userServices" data-toggle="modal" data-target="#emailModal">
                        <img src="{{asset('img/ico/mail.png')}}" alt="Get_Quote">
                           
                            <p>sales@decksys.com</p>
                        </div>
                      
                    </div>
					<div class="col-2 col-sm-1 col-md-2">
                        <div class="userServices" data-toggle="modal" data-target="#callUsModal">
                            <img src="{{asset('img/ico/chat.png')}}" alt="Get_Quote">
                            <p>Live Chat</p>
                        </div>
                       
                    </div>
                    <div class="col-2 col-sm-1 col-md-2 offset-md-6">
                        <div class="userServices" data-toggle="modal" data-target="#loginModal" style="border:none;">
                        <img src="{{asset('img/ico/login.png')}}" alt="Get_Quote">
                           
                            <p><a class="text" href="Register">Register </a>/ <a class="text" href="/login">Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div  class="navbarTwo">
            <nav class="navbar  navbar-expand-md navbar-light bgcolor">
              <a class="navbar-brand" href="/index">
              <img src="{{asset('img/logo/logo-decksys.png')}}" alt="logo" class="img-fluid img-logo">
               
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon "></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav ml-md-auto d-md-flex">
                  <li class="nav-item">
                    <a class="nav-link text-white" href="/VPS">SSD VPS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="/DedicatedServer">Dedicated Server</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="/ManagedServices">Managed Services</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="/support">Support</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">Blog</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="/locations">Datacenter Location</a>
                  </li>
                </ul>
              </div>
            </nav>

    </header>

  