<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script src="{{asset('js1/jquery-3.2.1.slim.min.js')}}"></script>
    <script src="{{asset('js1/bootstrap.min.js')}}"></script>
    <script src="{{asset('js1/decksysApp-v2.js')}}"></script>   
    <!--Custom Stylesheets-->
    <link rel="stylesheet" href="{{asset('css/style-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/mq-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/fs-v2.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{asset('js1/popper.min.js')}}"></script>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>    
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" type="text/css" /> -->
	

    <link rel="stylesheet" href="/css/font.css" type="text/css" />
    <link rel="stylesheet" href="/css/app.css" type="text/css" />
    <script src="{{asset('js1/StatesDropdown.js')}}"></script> 

 
    

    @yield('styles')
</head>
<body>
@include('partials.header')



@yield('content')

@include('partials.footer1')

@yield('scripts')



<script>
function myFunction1() {
    var ssdProduct1 = document.getElementById(id="ssdProduct1");
    if (ssdProduct1.checked == true){
        document.getElementById("opt6").disabled = true;
        document.getElementById("opt7").disabled = true; 
    } else{
        document.getElementById("opt6").disabled = false;
        document.getElementById("opt7").disabled = false;  
    }
}

</script>

<script>
function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("text");
  if (checkBox.checked == true){
    text.style.display = "inline-block";
  } else {
     text.style.display = "none";
  }

  var checkBox1 = document.getElementById("myCheck1");
  var text1 = document.getElementById("text1");
  if (checkBox1.checked == true){
    text1.style.display = "inline-block";
  } else {
     text1.style.display = "none";
  }

  var checkBox2 = document.getElementById("myCheck2");
  var text2 = document.getElementById("text2");
  if (checkBox2.checked == true){
    text2.style.display = "inline-block";
  } else {
     text2.style.display = "none";
  }

  var checkBox3 = document.getElementById("myCheck3");
  var text3 = document.getElementById("text3");
  if (checkBox3.checked == true){
    text3.style.display = "inline-block";
  } else {
     text3.style.display = "none";
  }

  var checkBox4 = document.getElementById("myCheck4");
  var text4 = document.getElementById("text4");
  if (checkBox4.checked == true){
    text4.style.display = "inline-block";
  } else {
     text4.style.display = "none";
  }

  var checkBox5 = document.getElementById("myCheck5");
  var text5 = document.getElementById("text5");
  if (checkBox5.checked == true){
    text5.style.display = "inline-block";
  } else {
     text5.style.display = "none";
  }
  var checkBox6 = document.getElementById("myCheck6");
  var text6 = document.getElementById("text6");
  if (checkBox6.checked == true){
    text6.style.display = "inline-block";
  } else {
     text6.style.display = "none";
  }

 var checkBox7 = document.getElementById("myCheck7");
  var text7 = document.getElementById("text7");
  if (checkBox7.checked == true){
    text7.style.display = "inline-block";
  } else {
     text7.style.display = "none";
  } 
 
  var checkBox8 = document.getElementById("myCheck8");
  var text8 = document.getElementById("text8");
  if (checkBox8.checked == true){
    text8.style.display = "inline-block";
  } else {
     text8.style.display = "none";
  } 

  var checkBox9 = document.getElementById("myCheck9");
  var text9 = document.getElementById("text9");
  if (checkBox9.checked == true){
    text9.style.display = "inline-block";
  } else {
     text9.style.display = "none";
  }

  var checkBox10 = document.getElementById("myCheck10");
  var text10 = document.getElementById("text10");
  if (checkBox10.checked == true){
    text10.style.display = "inline-block";
  } else {
     text10.style.display = "none";
  } 
}
</script>




</body>
</html>
